![Alt text](http://s3.postimg.org/hnkobv99v/uboxie_logo.png "Uboxie")

Uboxie project is, in its basic definition, a social music streaming application. Project consists 
of mobile and web component with almost same amount of functionality. Specific feature of 
uboxie is group music playing, i.e. all users inside specific group listen music from user
curated list. 
Uboxie tries to bring classic jukebox listening experience in modern form using music 
streaming and group listening. Average user must join one of the available groups or create 
his own group and choose song. Song is then added to the group playing queue and all users 
in this group are listening music from the playing queue

### What is this repository for? ###

This repository is for Uboxie Android application

### How do I get set up and compile/run this project? ###

* [Install Android studio](https://developer.android.com/sdk/installing/studio.html)
* Clone this project
* Import this project in Android studio using "import non-android studio project" option (as crazy as that seems)
* Run the "app" module

### How do I just test your app without compiling and running it? ###

Download [Uboxie.apk](https://bitbucket.org/dmacan23/uboxie-android/downloads/uboxie.apk?pk=519217) and install it on your device

### How do I compile and run this project in Eclipse? ###

You don't. This project was developed using Android Studio IDE and is not backwards-compatible with Eclipse project structure and it's build system.

### How do I use Uboxie? ###

You simply create group and add songs to the group. Doing those actions standalone on device has few bugs, so if you want, you can create group and add songs via web application on [uboxie.me](http://uboxie.me/) and listen to them on web or your device