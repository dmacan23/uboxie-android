package com.ingloriouscoders.uboxie;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.FrameLayout;

import com.ingloriouscoders.uboxie.fragment.MainFragment;

/**
 * Created by dmacan on 1/27/15.
 */
public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

    private Intent mLaunchIntent;
    private MainActivity mainActivity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mLaunchIntent = new Intent(getInstrumentation()
                .getTargetContext(), MainActivity.class);
        mainActivity = startActivity(mLaunchIntent, null, null);
    }

    public void testMainActivity() {
        assertNotNull("FragmentManager is null", mainActivity.getFragmentManager());
        FrameLayout frameLayout = (FrameLayout) mainActivity.findViewById(R.id.container);
        assertNotNull("Container is null", frameLayout);
        mainActivity.setupFragment(R.id.container, new MainFragment());
    }
}
