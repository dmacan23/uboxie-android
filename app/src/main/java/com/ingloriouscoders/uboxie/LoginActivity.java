package com.ingloriouscoders.uboxie;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.fragment.DeezerLoginFragment;
import com.ingloriouscoders.uboxie.fragment.LoginFragment;
import com.ingloriouscoders.uboxie.util.LightSerial;
import com.lightandroid.navigation.activity.LightActivity;

/**
 * Created by David on 8.12.2014..
 */
public class LoginActivity extends LightActivity {

    private LoginFragment loginFragment;

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_toolbar_container;
    }

    @Override
    public void main() {
        loginFragment = new DeezerLoginFragment();
        setupFragment(R.id.container, (Fragment) loginFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
