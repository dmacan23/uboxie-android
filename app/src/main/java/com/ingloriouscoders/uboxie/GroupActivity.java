package com.ingloriouscoders.uboxie;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.ingloriouscoders.uboxie.data.controller.GroupController;
import com.ingloriouscoders.uboxie.data.controller.PlayController;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.data.presenter.TrackPresenter;
import com.ingloriouscoders.uboxie.event.handlers.GroupActivityHandler;
import com.ingloriouscoders.uboxie.event.other.TrackChosen;
import com.ingloriouscoders.uboxie.event.player.ProgressUpdate;
import com.ingloriouscoders.uboxie.event.socket.OnTrackAddedEvent;
import com.ingloriouscoders.uboxie.fragment.AddTrackFragment;
import com.ingloriouscoders.uboxie.fragment.PlayerFragment;
import com.ingloriouscoders.uboxie.fragment.PlaylistFragment;
import com.ingloriouscoders.uboxie.fragment.ProgressFragment;
import com.ingloriouscoders.uboxie.network.USocket;
import com.ingloriouscoders.uboxie.util.DeviceUtil;
import com.lightandroid.navigation.activity.LightDrawerActivity;
import com.lightandroid.ui.drawer.DrawerSettings;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.List;

import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by David on 9.11.2014..
 * <p/>
 * This activity is opened after the user selects group and decides to play it
 * This activity manages PlayerFragment, PlaylistFragment, ProgressFragment and AddTrackFragment and their logic
 * It also contains PlayController and eases the control of the player instead of propagating that responsibility to other fragments
 */
public class GroupActivity extends LightDrawerActivity {

    private PlayController playController;
    private PlayerFragment playerFragment;
    private PlaylistFragment playlistFragment;
    private AddTrackFragment addTrackFragment;
    private ProgressFragment progressFragment;
    private String currentFragmentTag;
    private GroupActivityHandler groupActivityHandler;

    @Override
    public DrawerSettings setDrawerSettings() {
        return new DrawerSettings().dragMode(MenuDrawer.MENU_DRAG_WINDOW).layoutRes(R.layout.activity_container).position(Position.RIGHT).type(MenuDrawer.Type.OVERLAY).drawerSize(DeviceUtil.getPartOfScreenWidth(80, getBaseContext()));
    }

    @Override
    public void init(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            this.playController = new PlayController(this);
            GroupController.fetchGroup(getIntent());
            showProgress(getString(R.string.lbl_progress_group));
        }
    }

    @Override
    public void setupDrawerView(Bundle savedInstanceState, View menuView) {
        if (savedInstanceState == null) {
            if (groupActivityHandler == null)
                groupActivityHandler = new GroupActivityHandler(this).start();
        }
    }

    public void onGroupLoaded(Group group) {
        this.playController.setGroup(group);
        USocket.getInstance().emitGroupJoin(group, User.load(getBaseContext()));
        USocket.getInstance().listenOnTrackAdded(this);
        if (GroupController.hasTracks(group))
            this.showPlayer();
        else
            this.addTracks();
    }

    public void showPlayer() {
        if (playlistFragment == null)
            playlistFragment = new PlaylistFragment();
        setupFragment(getMenuDrawer().getMenuContainer().getId(), playlistFragment, PlaylistFragment.TAG);
        if (playerFragment == null)
            playerFragment = new PlayerFragment();
        setupFragment(getMenuDrawer().getContentContainer().getId(), playerFragment, PlayerFragment.TAG);
    }

    public void addTracks() {
        if (addTrackFragment == null)
            addTrackFragment = new AddTrackFragment();
        setupFragment(getMenuDrawer().getContentContainer().getId(), addTrackFragment, AddTrackFragment.TAG);
    }

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    public PlayerFragment getPlayerFragment() {
        return playerFragment;
    }

    public PlaylistFragment getPlaylistFragment() {
        return playlistFragment;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getMenuDrawer().closeMenu();
        getMenuDrawer().setMenuSize(DeviceUtil.getPartOfScreenWidth(70, getBaseContext()));
    }

    public PlayController getPlayController() {
        return playController;
    }

    public void onFragmentReady(Fragment fragment) {
        if (fragment instanceof PlaylistFragment) {
            playlistFragment.display(playController.getGroup().getSongsInGroup(), playController.getCurrentSongPosition());
            playController.play(true);
        } else if (fragment instanceof PlayerFragment) {
            Track current = GroupController.getCurrentTrack(playController.getGroup(), false);
            if (current != null && current.getKey() != 0)
                playerFragment.display(current);
        }
    }

    public void showProgress(String message) {
        if (progressFragment == null)
            progressFragment = new ProgressFragment();
        progressFragment.setMessage(message);
        setupFragment(getMenuDrawer().getContentContainer().getId(), progressFragment, ProgressFragment.TAG);
    }

    public void onProgressUpdate(ProgressUpdate progressUpdate) {
        if (playerFragment != null)
            playerFragment.progressUpdate(progressUpdate.getProgress());
    }

    public void onTrackChosen(TrackChosen trackChosen) {
        this.showPlayer();
    }

    @Override
    public void onBackPressed() {
        if (addTrackFragment != null && addTrackFragment.isVisible()) {
            if (playerFragment == null && playlistFragment == null) {
                closeGroup();
            } else
                showPlayer();
        } else if (getMenuDrawer().isMenuVisible())
            getMenuDrawer().closeMenu();
        else
            playerFragment.showDecision(R.string.lbl_decision_title, R.string.lbl_decision_message, R.string.lbl_negative, R.string.lbl_positive, onDialogDecisionClickListener);
    }

    private void closeActivity() {
        this.finish();
        playController.getUboxiePlayer().stop();
        groupActivityHandler.stop();
    }

    private CustomDialog.ClickListener onDialogDecisionClickListener = new CustomDialog.ClickListener() {

        @Override
        public void onConfirmClick() {
            closeActivity();
        }

        @Override
        public void onCancelClick() {

        }
    };

    public void onTrackAdded(OnTrackAddedEvent event) {
        if (playlistFragment == null)
            playlistFragment = new PlaylistFragment();
        playController.addTrack(event.getNewTrack());
        playlistFragment.addTrack(event.getNewTrack());
        printPlaylist(playController.getGroup().getSongsInGroup());
        printPlaylistDisplay(playlistFragment.getPlaylist());
        showPlayer();
    }

    private void printPlaylist(List<Track> playlist) {
//        for (Track t : playlist)
    }

    private void printPlaylistDisplay(List<TrackPresenter> presenters) {
//        for (TrackPresenter p : presenters)
//            ULog.d("TrackPresenter --> " + p.getTrack().getName());
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
    }

    private void closeGroup() {
        this.playController = null;
        this.playerFragment = null;
        this.addTrackFragment = null;
        this.currentFragmentTag = null;
        this.playlistFragment = null;
        this.progressFragment = null;
        groupActivityHandler.stop();
        groupActivityHandler = null;
        this.finish();
        System.gc();
    }
}
