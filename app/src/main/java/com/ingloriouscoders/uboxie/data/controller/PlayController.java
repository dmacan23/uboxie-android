package com.ingloriouscoders.uboxie.data.controller;

import android.app.Activity;
import android.content.Intent;

import com.ingloriouscoders.uboxie.core.deezer.DeezerPlayer;
import com.ingloriouscoders.uboxie.core.deezer.DeezerUserManager;
import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.core.musicapi.UboxieUserAuthenticator;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.player.action.PlayAction;
import com.ingloriouscoders.uboxie.event.player.action.TrackEndedAction;
import com.ingloriouscoders.uboxie.event.rest.TrackQueryCallback;
import com.ingloriouscoders.uboxie.event.rest.UpdateTrackCallback;
import com.ingloriouscoders.uboxie.network.request.UpdateTrackRequest;
import com.ingloriouscoders.uboxie.util.C;
import com.ingloriouscoders.uboxie.util.Uboxie;
import com.lightandroid.util.LightUtil;
import com.squareup.otto.Subscribe;

/**
 * Created by David on 14.12.2014..
 */
public class PlayController {

    private UboxiePlayer uboxiePlayer;
    private UboxieUserAuthenticator uboxieUserAuthenticator;
    private Group group;

    public PlayController(Activity activity, Group group) {
        UBus.registerSafe(this);
        this.group = group;
        this.uboxieUserAuthenticator = new DeezerUserManager(activity);
        this.uboxiePlayer = DeezerPlayer.getInstance(((DeezerUserManager) uboxieUserAuthenticator).getDeezerConnect(), activity);
    }

    public PlayController(Activity activity) {
        this(activity, new Group());
    }

    public PlayController(Activity activity, Intent intent) {
        this(activity, Uboxie.deserialize(intent.getStringExtra(C.Key.GROUP_SELECTED), Group.class));
    }

    /**
     * Getter for the UboxiePLayer object
     *
     * @return UboxiePlayer attribute
     */
    public UboxiePlayer getUboxiePlayer() {
        return uboxiePlayer;
    }

    /**
     * Loads array of tracks and status message
     *
     * @param query Parameter for querying tracks
     * @param mask  Request mask
     */
    private void queryTracks(String query, String mask) {
        Uboxie.API().trackQuery(query, C.AUTOCOMPLETE_RESULT_NUMBER, "", TrackQueryCallback.getInstance());
    }

    /**
     * Loads array of tracks for autocomplete field
     *
     * @param query Parameter for querying tracks
     */
    public void queryAutocomplete(String query) {
        queryTracks(query, C.Api.QUERY_TRACK);
    }

    /**
     * Loads specific track
     *
     * @param query Parameter for querying track
     */
    public void queryTrack(String query) {
        queryTracks(query, C.Api.QUERY_TRACK);
    }

    /**
     * Getter for Group object
     *
     * @return currently selected group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Setter for Group object
     *
     * @param group Group to be set
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * Getter for UboxieUserAuthenticator object
     *
     * @return Instantiated UboxieUserAuthenticator
     */
    public UboxieUserAuthenticator getUboxieUserAuthenticator() {
        return uboxieUserAuthenticator;
    }

    /**
     * Adds track to current group
     *
     * @param track Track to be added
     */
    public void addTrack(Track track) {
        boolean active = !GroupController.addTrackToGroup(group, track);
        Track current = GroupController.getCurrentTrack(group, false);
        if (!active)
            play(current, false);
    }

    /**
     * After the track is finished with playing, this method is called
     *
     * @param action Action object containing finished track information
     */
    @Subscribe
    public void onTrackEnded(TrackEndedAction action) {
        int currentPosition = group.getCurrentSongPosition();
        group.incrementCurrentSongPosition();
        if (currentPosition < group.getCurrentSongPosition())
            play(false);
    }

    /**
     * After the group is loaded, this method is called
     *
     * @param group Group loaded
     */
    @Subscribe
    public void onGroupLoaded(Group group) {
        GroupController.processGroup(group);
        this.group = group;
    }

    /**
     * Plays group current track
     */
    public void play(boolean offset) {
        Track trackToPlay = GroupController.getCurrentTrack(group, false);
        if (group != null && trackToPlay != null && uboxiePlayer != null && !uboxiePlayer.isPlaying() && !trackToPlay.isFinishedPlaying())
            this.play(trackToPlay, offset);
    }

    /**
     * Plays given track with specified offset
     *
     * @param track  Track to be played
     * @param offset Offset for the track
     */
    private void play(Track track, boolean offset) {
        int offsetSeconds = 0;
        if (offset && track != null)
            offsetSeconds = LightUtil.castLongToIntSafe(track.getStartTimeOffset());
        if (track != null) {
            this.uboxiePlayer.play(track, offsetSeconds);
            updateCurrentSong(track);
            UBus.getInstance().post(new PlayAction(uboxiePlayer, track, offsetSeconds, getCurrentSongPosition()));
        }
        updateCurrentSong(track);
    }

    //    @Subscribe
    public void onSeekFailed() {
        Track currentTrack = GroupController.getNextTrack(group);
        play(currentTrack, false);
    }

    /**
     * Returns position of the current track
     *
     * @return Value of current track position
     */
    public int getCurrentSongPosition() {
        return GroupController.getCurrentTrackPosition(group);
    }

    public void updateCurrentSong(Track track) {
        Uboxie.API().updateCurrentTrack(this.getGroup().getId(), new UpdateTrackRequest(track.getId()), UpdateTrackCallback.getInstance());
    }
}
