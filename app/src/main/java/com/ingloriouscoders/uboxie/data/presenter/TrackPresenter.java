package com.ingloriouscoders.uboxie.data.presenter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.event.UBus;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by David on 15.10.2014..
 */
public class TrackPresenter implements com.ingloriouscoders.uboxie.ui.LightRecyclerPresenter, View.OnClickListener {

    @InjectView(R.id.imgAuthor)
    ImageView imgAlbum;
    @InjectView(R.id.txtPlaylistTrackName)
    TextView txtTrackName;
    @InjectView(R.id.txtPlaylistArtistName)
    TextView txtTrackAuthor;

    private Track track;
    private View view;
    private boolean enableColorization;
    private boolean isActive;

    public TrackPresenter(Track track) {
        this.track = track;
        this.enableColorization = true;
    }

    public TrackPresenter(Track track, boolean enableColorization) {
        this.track = track;
        this.enableColorization = enableColorization;
    }

    @Override
    public void inject(View view) {
        ButterKnife.inject(this, view);
        this.view = view;
        this.view.setOnClickListener(this);
    }

    @Override
    public void display(View view, int position) {
        Picasso.with(view.getContext()).load(track.getIcon()).into(imgAlbum);
        txtTrackName.setText(track.getName());
        txtTrackAuthor.setText(track.getArtist());
        colorizeAccordingly();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_playlist;
    }


    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    /**
     * Toggles track presenter active. If the presenter is active, it is being colorized to display track selection. If the presenter is not active, it's color returns to normal state
     *
     * @param isActive If true, set highlight color. If false, return default color
     * @return Modified TrackPresenter object
     */
    public TrackPresenter setActive(boolean isActive) {
        this.isActive = isActive;
        if (view != null)
            colorizeAccordingly();
        return this;
    }


    /**
     * Sets background and text color to given view
     *
     * @param view          View being colorized
     * @param backgroundRes Resource identifier for background color
     * @param textRes       Resource identifier for text color
     */
    private void colorize(View view, int backgroundRes, int textRes) {
        view.setBackgroundColor(view.getResources().getColor(backgroundRes));
        txtTrackAuthor.setTextColor(view.getResources().getColor(textRes));
        txtTrackName.setTextColor(view.getResources().getColor(textRes));
    }

    /**
     * If the track presenter has colorization enabled and is active, then it is colorized to highlighted color. If not, its background and text color are returned to normal
     */
    private void colorizeAccordingly() {
        if (this.isActive && enableColorization)
            colorize(view, R.color.playlist_active_track, R.color.playlist_active_text);
        else if (!this.isActive && enableColorization)
            colorize(view, R.color.playlist_inactive_track, R.color.playlist_inactive_text);
    }

    @Override
    public void onClick(View v) {
        UBus.getInstance().post(this);
    }
}
