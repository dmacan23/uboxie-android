package com.ingloriouscoders.uboxie.data.controller;

import android.content.Context;
import android.content.Intent;

import com.ingloriouscoders.uboxie.GroupActivity;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.data.presenter.GroupPresenter;
import com.ingloriouscoders.uboxie.event.rest.CreateGroupCallback;
import com.ingloriouscoders.uboxie.event.rest.GroupCallback;
import com.ingloriouscoders.uboxie.event.rest.GroupsCallback;
import com.ingloriouscoders.uboxie.network.response.GroupsResponse;
import com.ingloriouscoders.uboxie.util.C;
import com.ingloriouscoders.uboxie.util.Uboxie;

import java.util.List;
import java.util.Random;

/**
 * Created by David on 9.11.2014..
 */
public class GroupController {

    /**
     * Processes API response by selecting the current track in all groups
     *
     * @param response API response containing array of groups, amongst other attributes
     * @return Processed array of groups
     */
    public static Group[] processResponse(GroupsResponse response) {
        Group[] groups = response.getGroups();
//        for (Group group : groups)
//            processGroup(group);
        return groups;
    }

    /**
     * Processes given Group by selecting the current track within it
     *
     * @param group Group being processed
     * @return Processed group
     */
    public static Group processGroup(Group group) {
        boolean allFinished = true;
        if (group != null && group.getSongsInGroup() != null) {
            for (int i = 0; i < group.getSongsInGroup().size(); i++) {
                if (group.getSongsInGroup().get(i).getId().equals(group.getCurrentTrack().getId())) {
                    group.setCurrentSongPosition(i);
                    allFinished = false;
                    break;
                }
            }
            if (allFinished)
                group.setCurrentSongPosition(-1);
        }
        return group;
    }

    /**
     * Sends POST request to create new group
     *
     * @param groupName Name of the group which is to be created
     */
    public static void createGroup(String groupName, Context context) {
        Uboxie.API().createGroup(groupName, User.load(context).getToken(), CreateGroupCallback.getInstance());
    }

    /**
     * Loads array of groups
     *
     * @param startPosition start index of the array
     * @param endPosition   end index of the array
     */
    public static void getGroups(int startPosition, int endPosition) {
        Uboxie.API().getGroups(startPosition, endPosition, GroupsCallback.getInstance());
    }

    /**
     * Appends track to the group
     *
     * @param group Group being appended to
     * @param track Track being appended
     */
    public static boolean addTrackToGroup(Group group, Track track) {
        boolean newIsCurrent = false;
        if (group != null && track != null && group.getSongsInGroup() != null) {
            group.getSongsInGroup().add(track);
        }
        Track current = GroupController.getCurrentTrack(group, false);
        if ((group != null && group.getSongsInGroup() != null && current != null && current.isFinishedPlaying()) || (current == null && group != null && group.getSongsInGroup() != null)) {
            group.setCurrentSongPosition(group.getSongsInGroup().size() - 1);
            newIsCurrent = true;
        }
        return newIsCurrent;
    }

    /**
     * Fetches current track from the group and (if specified) increments current song position by one
     *
     * @param group       Group from which the track is being fetched
     * @param moveForward If true, groups current track position is being incremented by one, meaning that the next time current track is being fetched, it will be the next one
     * @return Current track from the group
     */
    public static Track getCurrentTrack(Group group, boolean moveForward) {
        Track retVal = group.getCurrentSong();
        if (moveForward)
            group.incrementCurrentSongPosition();
        return retVal;
    }

    /**
     * Fetches next track from the group and recalculates current track position
     *
     * @param group Group from which the track is being fetched
     * @return Next track from the group
     */
    public static Track getNextTrack(Group group) {
        if ((group.getCurrentSongPosition() - 1) < group.getSongsInGroup().size()) {
            Track retVal = group.getSongsInGroup().get(group.getCurrentSongPosition() + 1);
            group.setCurrentSongPosition(group.getCurrentSongPosition() + 1);
        }
        return null;
    }

    /**
     * Fetches cover image for the group
     *
     * @param group Group from which the cover image is being fetched
     * @return URL to the groups cover image, null if the current track does not exist
     */
    public static String getCoverImage(Group group) {
        if (group != null)
            return group.getPicture();
        return null;
    }

    /**
     * Returns name for the representation of the group
     *
     * @param group Group from which the name is being fetched
     * @return Name of the group
     */
    public static String getGroupName(Group group) {
        if (group != null)
            return group.getName();
        return null;
    }

    /**
     * Counts number of listeners within group
     *
     * @param group Group from which the number of listeners is being fetched
     * @return Number of listeners within group
     */
    public static int getGroupNumberOfListeners(Group group) {
        return new Random().nextInt(10) + 1;
    }

    /**
     * Fetches all tracks from the group
     *
     * @param group Group from which the playlist is being fetched
     * @return ArrayList of tracks representing playlist
     */
    public static List<Track> getPlaylist(Group group) {
        return group.getSongsInGroup();
    }

    /**
     * Returns the position of currently playing track
     *
     * @param group Group from which the current position is being fetched
     * @return position of currently playing track
     */
    public static int getCurrentTrackPosition(Group group) {
        return group.getCurrentSongPosition();
    }

    /**
     * Fetches the id of the group from GroupPresenter
     *
     * @param groupPresenter Presenter containing the group
     * @return Id of the group
     */
    public static String getGroupId(GroupPresenter groupPresenter) {
        return groupPresenter.getGroup().getId();
    }

    /**
     * Creates intent for opening GroupActivity with selected group
     *
     * @param group   the desired group
     * @param context Context needed for creating intent
     * @return Fully prepared intent for opening GroupActivity with necessary parameters
     */
    public static Intent createOpenGroupIntent(Group group, Context context) {
        Intent intent = new Intent(context, GroupActivity.class);
        intent.putExtra(C.Key.GROUP_SELECTED, group.getId());
        return intent;
    }

    /**
     * Creates GET request for fetching group with specified ID within given Intent
     *
     * @param intent Intent containing the ID of the group to be fetched
     */
    public static void fetchGroup(Intent intent) {
        fetchGroup(intent.getStringExtra(C.Key.GROUP_SELECTED));
    }

    /**
     * Creates GET request for fetching group with specified ID
     *
     * @param groupId ID of the group to be fetched
     */
    public static void fetchGroup(String groupId) {
        Uboxie.API().getGroup(groupId, GroupCallback.getInstance());
    }

    /**
     * Checks if the group has track previous to the current one
     *
     * @param group Group being checked
     * @return True if group has a previous track, false if not
     */
    public static boolean hasPreviousTrack(Group group) {
        return group.getCurrentSongPosition() > 0;
    }

    /**
     * Checks if the group has tracks that have not yet been played
     *
     * @param group Group being checked
     * @return True if group has unplayed tracks, false if not
     */
    public static boolean isGroupActive(Group group) {
        for (Track track : group.getSongsInGroup())
            if (!track.isFinished() && !group.getSongsInGroup().get(group.getSongsInGroup().size() - 1).isFinishedPlaying())
                return true;
        return false;
    }

    /**
     * Checks if the group has tracks
     *
     * @param group Group being checked
     * @return True if group has at leas one track, false if not
     */
    public static boolean hasTracks(Group group) {
        return group != null && group.getSongsInGroup() != null && (group.getSongsInGroup().size() > 0);
    }
}
