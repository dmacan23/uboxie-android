package com.ingloriouscoders.uboxie.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by David on 6.10.2014..
 */
public class Group extends UboxieModel {

    @Expose
    private String name;
    @Expose
    private List<User> users;
    @Expose
    @SerializedName("listenerNumber")
    private int listenerNumber;
    @Expose
    private String created;
    @Expose
    @SerializedName("songsInGroup")
    private List<Track> songsInGroup;
    @Expose
    private double rating;
    @Expose
    private Track currentSong;
    private int currentSongPosition;
    @Expose
    private User owner;

    public Group() {
        currentSongPosition = -1;
    }

    public Group(String name, String picture, List<User> users) {
        this();
        this.name = name;
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        if (currentSong != null)
            return currentSong.getIcon();
        return null;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getListenerNumber() {
        return listenerNumber;
    }

    public void setListenerNumber(int listenerNumber) {
        this.listenerNumber = listenerNumber;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public List<Track> getSongsInGroup() {
        return songsInGroup;
    }

    public void setSongsInGroup(List<Track> songsInGroup) {
        this.songsInGroup = songsInGroup;
    }

    public Track getCurrentSong() {
        if (currentSongPosition < songsInGroup.size() && currentSongPosition >= 0)
            return songsInGroup.get(currentSongPosition);
        return null;
    }

    public Track getCurrentTrack() {
        return this.currentSong;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getCurrentSongPosition() {
        return currentSongPosition;
    }

    public void setCurrentSongPosition(int currentSongPosition) {
        this.currentSongPosition = currentSongPosition;
    }

    public void incrementCurrentSongPosition() {
        if (currentSongPosition < (songsInGroup.size() - 1))
            this.currentSongPosition += 1;
    }

    public void setCurrentSong(Track currentSong) {
        this.currentSong = currentSong;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
