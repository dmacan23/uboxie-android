package com.ingloriouscoders.uboxie.data.presenter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.controller.GroupController;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.event.RippleDelayClick;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by David on 6.10.2014..
 */
public class GroupPresenter implements com.ingloriouscoders.uboxie.ui.LightRecyclerPresenter, View.OnClickListener {

    @InjectView(R.id.imgGroupCover)
    ImageView imgGroup;
    @InjectView(R.id.txtGroupName)
    TextView txtGroupName;
    @InjectView(R.id.txtListenerNumber)
    TextView txtListenerNumber;
    @InjectView(R.id.ripple)
    RippleView rippleView;

    private Group group;


    public GroupPresenter(Group group) {
        this.group = group;
    }

    @Override
    public void inject(View view) {
        ButterKnife.inject(this, view);
        rippleView.setOnClickListener(this);
    }

    @Override
    public void display(View view, int position) {
        Picasso.with(view.getContext()).load(GroupController.getCoverImage(group)).placeholder(R.drawable.ic_launcher).error(R.drawable.ic_launcher).into(imgGroup);
        txtGroupName.setText(GroupController.getGroupName(group));
        txtListenerNumber.setText(String.valueOf(GroupController.getGroupNumberOfListeners(group)));
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_group;
    }

    public Group getGroup() {
        return group;
    }

    /**
     * After the group is clicked, delay click action for ripple duration seconds
     *
     * @param v View that got clicked
     */
    @Override
    public void onClick(View v) {
        RippleDelayClick.delayClick(this, 700);
    }

    public void updateGroup(Group group) {
        this.group = group;
    }
}
