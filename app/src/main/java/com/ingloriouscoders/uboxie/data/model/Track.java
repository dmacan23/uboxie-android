package com.ingloriouscoders.uboxie.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ingloriouscoders.uboxie.core.deezer.Deezer;
import com.ingloriouscoders.uboxie.util.C;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by David on 15.10.2014..
 */
public class Track extends UboxieModel {

    @Expose
    private long key;
    @Expose
    private int duration;
    @Expose
    @SerializedName("startTime")
    private String startTime;
    @Expose
    private String artist;
    @Expose
    private boolean finished;
    @Expose
    private String icon;
    @Expose
    private String info;
    @Expose
    private String name;

    public Track() {
    }

    private boolean isPlaying;

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getStartTimeString() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public DateTime getStartTime() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(C.DATETIME_MONGO);
        if (getStartTimeString() == null)
            return new DateTime();
        return formatter.parseDateTime(this.getStartTimeString());
    }

    public long getStartTimeTimestamp() {
        return getStartTime().getMillis();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public long getStartTimeOffset() {
        return calculateOffset(getStartTimeTimestamp()) + Deezer.OFFSET;
    }

    public static long calculateOffset(long timestamp) {
        DateTime dt = new DateTime(DateTimeZone.UTC);
        return dt.getMillis() - timestamp;
    }

    public boolean isFinishedPlaying() {
        return (getStartTimeOffset() / 1000) > this.duration;
    }

    public boolean isPlaying() {
        long mills = new DateTime(DateTimeZone.UTC).getMillis();
        this.isPlaying = (!this.finished) && (this.getStartTimeTimestamp() < new DateTime(DateTimeZone.UTC).getMillis());
        return isPlaying;
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }


}
