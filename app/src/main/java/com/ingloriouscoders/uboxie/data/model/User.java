package com.ingloriouscoders.uboxie.data.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ingloriouscoders.uboxie.util.C;
import com.ingloriouscoders.uboxie.util.LightSerial;
import com.ingloriouscoders.uboxie.util.Uboxie;

public class User {

    @Expose
    private int id;
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastName;
    @SerializedName("firstname")
    @Expose
    private String firstName;
    @Expose
    private String birthday;
    @SerializedName("inscription_date")
    @Expose
    private String inscriptionDate;
    @Expose
    private String gender;
    @Expose
    private String link;
    @Expose
    private String picture;
    @Expose
    private String country;
    @Expose
    private String lang;
    @SerializedName("tracklist")
    @Expose
    private String trackList;
    @Expose
    private String type;
    @Expose
    private int status;
    @Expose
    private String token;
    @Expose
    private String tokenSecret;

    private boolean authenticated;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, String avatar) {
        this.name = name;
        this.picture = avatar;
    }

    /**
     * Loads user from the preferences
     *
     * @param context Context for fetching the SharedPreferences
     * @return User stored in the preferences
     */
    public static User load(Context context) {
        return LightSerial.deserialize(Uboxie.loadPreferences(context, C.Key.PREFERENCES).getString(C.Key.USER, null), User.class);
    }

    public static boolean clear(Context context) {
        new User().store(context);
        return true;
    }

    /**
     * Stores user in the preferences
     *
     * @param context Context for fetching the SharedPreferences
     */
    public void store(Context context) {
        SharedPreferences.Editor editor = Uboxie.loadEditor(context, C.Key.PREFERENCES).putString(C.Key.USER, LightSerial.serialize(this));
        editor.commit();
    }


    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstname The firstname
     */
    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    /**
     * @return The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return The inscriptionDate
     */
    public String getInscriptionDate() {
        return inscriptionDate;
    }

    /**
     * @param inscriptionDate The inscription_date
     */
    public void setInscriptionDate(String inscriptionDate) {
        this.inscriptionDate = inscriptionDate;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return The picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * @param picture The picture
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * @param lang The lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * @return The tracklist
     */
    public String getTrackList() {
        return trackList;
    }

    /**
     * @param tracklist The tracklist
     */
    public void setTrackList(String tracklist) {
        this.trackList = tracklist;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return Uboxie.serialize(this);
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }
}
