package com.ingloriouscoders.uboxie.data.controller;

import android.content.Context;

import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.event.rest.AddTrackCallback;
import com.ingloriouscoders.uboxie.event.rest.TrackQueryCallback;
import com.ingloriouscoders.uboxie.event.rest.UpdateTrackCallback;
import com.ingloriouscoders.uboxie.network.request.UpdateTrackRequest;
import com.ingloriouscoders.uboxie.util.Uboxie;

/**
 * Created by dmacan on 12/27/14.
 */
public class TrackController {

    private static boolean isSearching;

    /**
     * Adds track to group using web API
     *
     * @param group Group in which the track is being added
     * @param track Track being added
     */
    public static void addTrackToGroup(Group group, Track track, Context context) {
        Uboxie.API().addTrackToGroup(group.getId(), track.getDuration(), track.getName(), track.getKey(), User.load(context).getToken(), track.getInfo(), track.getArtist(), track.getIcon(), AddTrackCallback.getInstance());
    }

    public static void updateCurrentSong(Group group, Track track) {
        UpdateTrackRequest request = new UpdateTrackRequest(track.getId());
        Uboxie.API().updateCurrentTrack(group.getId(), request.getTrackId(), request.getStartTime(), UpdateTrackCallback.getInstance());
    }

    /**
     * Query tracks from the API
     *
     * @param query Search query
     */
    public void queryTracks(String query, Context context) {
        if (!isSearching) {
            isSearching = true;
            Uboxie.API().trackQuery(query, 20, User.load(context).getToken(), TrackQueryCallback.getInstance(this));
        }
    }

    /**
     * Sets searching flag for easier player manipulation
     *
     * @param isSearching If true, tracks are being searched. If false, tracks are not being searched;
     */
    public void setSearching(boolean isSearching) {
        TrackController.isSearching = isSearching;
    }

}
