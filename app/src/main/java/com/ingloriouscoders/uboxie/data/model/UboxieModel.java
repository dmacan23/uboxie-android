package com.ingloriouscoders.uboxie.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lightandroid.type.LightData;

/**
 * Created by David on 6.12.2014..
 */
public class UboxieModel extends LightData {

    @Expose
    @SerializedName("_id")
    private String id;
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
