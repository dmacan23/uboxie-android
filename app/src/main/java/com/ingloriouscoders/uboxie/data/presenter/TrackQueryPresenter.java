package com.ingloriouscoders.uboxie.data.presenter;

import android.view.View;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.lightandroid.ui.presenter.LightAdapterItemFilterable;

/**
 * Created by David on 9.11.2014..
 */
public class TrackQueryPresenter implements LightAdapterItemFilterable {

    private TextView txtArtist;
    private TextView txtSong;

    private Track track;

    public TrackQueryPresenter(Track track) {
        this.track = track;
    }

    @Override
    public void display(View view, int position) {
        txtArtist = (TextView) view.findViewById(R.id.txtTrackArtist);
        txtSong = (TextView) view.findViewById(R.id.txtTrackName);
        txtArtist.setText(track.getArtist());
        txtSong.setText(track.getName());
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.item_track_query;
    }

    @Override
    public String[] getFilterableData() {
        return new String[]{track.getName()};
    }
}
