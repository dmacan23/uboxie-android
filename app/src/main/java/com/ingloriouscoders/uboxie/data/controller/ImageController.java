package com.ingloriouscoders.uboxie.data.controller;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.ingloriouscoders.uboxie.R;
import com.lightandroid.util.blur.LightBlurTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by David on 9.11.2014..
 */
public class ImageController implements LightBlurTask.BlurListener {

    private ImageView imgAlbumCoverBlurred;
    private ImageView imgAlbumCover;

    /**
     * Blurs image asynchronously from given ImageView
     *
     * @param imgAlbumCover ImageView containing the drawable to blur
     */
    public void blurImage(ImageView imgAlbumCover) {
        LightBlurTask task = new LightBlurTask();
        task.setOnBlurListener(this);
        task.setRadius(10);
        task.execute(((BitmapDrawable) imgAlbumCover.getDrawable()).getBitmap());
    }

    /**
     * Loads blurred image into one ImageView and non-blurred into another
     *
     * @param url                  Url to fetch image from
     * @param imgAlbumCoverBlurred Album cover ImageView containing the blurred version of image
     * @param imgAlbumCover        Album cover ImageView containing the original version of image
     */
    public void loadImage(String url, ImageView imgAlbumCoverBlurred, ImageView imgAlbumCover) {
        this.imgAlbumCoverBlurred = imgAlbumCoverBlurred;
        this.imgAlbumCover = imgAlbumCover;
        Picasso.with(imgAlbumCover.getContext()).load(url).into(imgAlbumCover, loadCallback);
    }

    /**
     * Called when blur process starts preparing
     */
    @Override
    public void onBlurPrepare() {
    }

    /**
     * Called after the blur process is completed
     *
     * @param bitmap Blurred Bitmap image
     */
    @Override
    public void onBlurFinish(Bitmap bitmap) {
        Animation fadeIn = AnimationUtils.loadAnimation(imgAlbumCoverBlurred.getContext(), R.anim.fade_in);
        imgAlbumCoverBlurred.startAnimation(fadeIn);
        imgAlbumCoverBlurred.setImageBitmap(bitmap);
    }


    /**
     * Callback for loading image
     */
    private Callback loadCallback = new Callback() {
        @Override
        public void onSuccess() {
            blurImage(imgAlbumCover);
        }

        @Override
        public void onError() {

        }
    };

}
