package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.data.controller.GroupController;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.network.response.GroupResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 12/28/14.
 */
public class GroupCallback implements Callback<GroupResponse> {

    private static GroupCallback groupCallback;

    private GroupCallback() {

    }

    public static GroupCallback getInstance() {
        if (groupCallback == null)
            groupCallback = new GroupCallback();
        return groupCallback;
    }

    @Override
    public void success(GroupResponse groupResponse, Response response) {
        try {
            UBus.getInstance().post(GroupController.processGroup(groupResponse.getGroups()[0]));
        } catch (Exception e) {
            UBus.getInstance().post(new UboxieError("Group response error"));
        }
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(new UboxieError(error.getMessage()));
    }
}
