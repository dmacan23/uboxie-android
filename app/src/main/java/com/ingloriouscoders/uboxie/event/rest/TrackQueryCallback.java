package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.data.controller.TrackController;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.network.response.TrackQueryResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 12/24/14.
 */
public class TrackQueryCallback implements Callback<TrackQueryResponse> {

    private static TrackQueryCallback trackQueryCallback;
    private TrackController trackController;

    private TrackQueryCallback() {
    }

    public static TrackQueryCallback getInstance() {
        if (trackQueryCallback == null)
            trackQueryCallback = new TrackQueryCallback();
        return trackQueryCallback;
    }

    public static TrackQueryCallback getInstance(TrackController trackController) {
        TrackQueryCallback retVal = getInstance();
        retVal.setTrackController(trackController);
        return retVal;
    }

    public TrackController getTrackController() {
        return trackController;
    }

    public void setTrackController(TrackController trackController) {
        this.trackController = trackController;
    }

    @Override
    public void success(TrackQueryResponse trackQueryResponse, Response response) {
        if (trackController != null)
            trackController.setSearching(false);
        if (trackQueryResponse.getStatus().equals("true") && trackQueryResponse.getResult() != null && (trackQueryResponse.getResult().length > 0) && trackQueryResponse.getResult()[0] != null)
            UBus.getInstance().post(trackQueryResponse);
        else
            UBus.getInstance().post(new UboxieError("A problem has occurred"));
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(error);
    }
}
