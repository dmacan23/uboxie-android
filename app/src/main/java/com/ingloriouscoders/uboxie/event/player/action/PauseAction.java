package com.ingloriouscoders.uboxie.event.player.action;

import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/28/14.
 */
public class PauseAction {

    private Track track;
    private UboxiePlayer player;
    private long position;

    public PauseAction(Track track, UboxiePlayer player, long position) {
        this.track = track;
        this.player = player;
        this.position = position;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public UboxiePlayer getPlayer() {
        return player;
    }

    public void setPlayer(UboxiePlayer player) {
        this.player = player;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }
}
