package com.ingloriouscoders.uboxie.event.socket;

import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.util.Uboxie;

/**
 * Created by David on 25.1.2015..
 */
public class OnGroupAddedEvent {

    private Object[] args;
    private Group newGroup;

    public OnGroupAddedEvent(Object[] args) {
        this.args = args;
        String arg = args[0].toString();
        this.newGroup = Uboxie.deserialize(arg, Group.class);
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Group getNewGroup() {
        return newGroup;
    }
}
