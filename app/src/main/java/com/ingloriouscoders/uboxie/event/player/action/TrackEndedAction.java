package com.ingloriouscoders.uboxie.event.player.action;

import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/28/14.
 */
public class TrackEndedAction {

    private Track track;
    private UboxiePlayer player;
    private int position;

    public TrackEndedAction(Track track, UboxiePlayer player) {
        this.track = track;
        this.player = player;
    }

    public TrackEndedAction(Track track, UboxiePlayer player, int position) {
        this.track = track;
        this.player = player;
        this.position = position;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public UboxiePlayer getPlayer() {
        return player;
    }

    public void setPlayer(UboxiePlayer player) {
        this.player = player;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
