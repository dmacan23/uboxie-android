package com.ingloriouscoders.uboxie.event.other;

import com.ingloriouscoders.uboxie.data.model.User;

/**
 * Created by dmacan on 12/25/14.
 */
public class UserAuthentication {

    private User user;
    private boolean success;

    public UserAuthentication(User user, boolean success) {
        this.user = user;
        this.success = success;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}

