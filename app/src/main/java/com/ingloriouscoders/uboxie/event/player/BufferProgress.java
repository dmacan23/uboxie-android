package com.ingloriouscoders.uboxie.event.player;

import android.app.Activity;

import com.ingloriouscoders.uboxie.core.deezer.DeezerPlayer;
import com.ingloriouscoders.uboxie.event.UBus;

/**
 * Created by dmacan on 12/25/14.
 */
public class BufferProgress extends PlayerHandlerEvent implements Runnable {

    private int counter;
    private double progressPercentage;

    public BufferProgress(Activity activity, DeezerPlayer deezerPlayer) {
        super(activity, deezerPlayer);
    }

    @Override
    public void run() {
        synchronized (this) {
            if (getDeezerPlayer().isSeeking() && getDeezerPlayer().isSeekable(progressPercentage)) {
                counter++;
                if (counter > 1) {
                    counter = 0;
                    getDeezerPlayer().setSeeking(false);
                    UBus.getInstance().post(new SeekPosition(progressPercentage, true));
                }
            }
        }
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public double getProgressPercentage() {
        return progressPercentage;
    }

    public void setProgressPercentage(double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }
}
