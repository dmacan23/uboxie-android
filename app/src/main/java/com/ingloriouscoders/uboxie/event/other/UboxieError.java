package com.ingloriouscoders.uboxie.event.other;

import retrofit.RetrofitError;

/**
 * Created by dmacan on 12/24/14.
 */
public class UboxieError {

    private String description;

    public UboxieError() {
    }

    public UboxieError(RetrofitError error) {
        this.description = error.getMessage();
    }

    public UboxieError(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
