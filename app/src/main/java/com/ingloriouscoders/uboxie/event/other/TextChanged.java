package com.ingloriouscoders.uboxie.event.other;

/**
 * Created by dmacan on 12/27/14.
 */
public class TextChanged {

    private String text;
    private boolean isFinal;

    public TextChanged(String text) {
        this.text = text;
        this.isFinal = false;
    }

    public TextChanged(String text, boolean isFinal) {
        this.text = text;
        this.isFinal = isFinal;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }
}
