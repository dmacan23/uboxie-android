package com.ingloriouscoders.uboxie.event.player;

import android.app.Activity;

import com.ingloriouscoders.uboxie.core.deezer.DeezerPlayer;
import com.ingloriouscoders.uboxie.event.UBus;

/**
 * Created by dmacan on 12/25/14.
 */
public class PlayerProgress extends PlayerHandlerEvent implements Runnable {

    private ProgressUpdate progressUpdate;

    public PlayerProgress(Activity activity, DeezerPlayer deezerPlayer) {
        super(activity, deezerPlayer);
        this.progressUpdate = new ProgressUpdate();
    }

    @Override
    public void run() {
        synchronized (this) {
            UBus.getInstance().post(progressUpdate);
        }
    }

    public void setProgress(long progress) {
        progressUpdate.setProgress(progress);
    }

}
