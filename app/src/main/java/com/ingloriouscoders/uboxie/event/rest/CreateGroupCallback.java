package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.network.USocket;
import com.ingloriouscoders.uboxie.network.response.GroupCreatedResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 12/26/14.
 */
public class CreateGroupCallback implements Callback<GroupCreatedResponse> {

    private static CreateGroupCallback createGroupCallback;

    private CreateGroupCallback() {

    }

    public static CreateGroupCallback getInstance() {
        if (createGroupCallback == null)
            createGroupCallback = new CreateGroupCallback();
        return createGroupCallback;
    }

    @Override
    public void success(GroupCreatedResponse groupCreatedResponse, Response response) {
        if (groupCreatedResponse.getStatus().equals("true")) {
            UBus.getInstance().post(groupCreatedResponse);
            USocket.getInstance().emitGroupCreate(groupCreatedResponse.getGroup());
        }
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(error);
    }
}
