package com.ingloriouscoders.uboxie.event;


import android.os.Handler;

/**
 * Created by dmacan on 12/26/14.
 */
public class RippleDelayClick {

    private static Task task;
    private static Handler handler;

    public static void delayClick(Object object, long mills) {
        if (task == null)
            task = new Task();
        task.setObject(object);
        if (handler == null)
            handler = new Handler();
        handler.postDelayed(task, mills);
    }

    private static class Task implements Runnable {

        private Object object;

        @Override
        public void run() {
            if (object != null)
                UBus.getInstance().post(object);
        }

        public void setObject(Object object) {
            this.object = object;
        }
    }

}
