package com.ingloriouscoders.uboxie.event.socket;

/**
 * Created by David on 25.1.2015..
 */
public class OnConnectEvent {
    private Object[] args;

    public OnConnectEvent(Object[] args) {
        this.args = args;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }
}
