package com.ingloriouscoders.uboxie.event.click;

import com.ingloriouscoders.uboxie.event.UBus;

import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by dmacan on 12/26/14.
 */
public class DialogClickListener implements CustomDialog.ClickListener {

    private CustomDialog customDialog;

    public DialogClickListener(CustomDialog customDialog) {
        this.customDialog = customDialog;
    }

    @Override
    public void onConfirmClick() {
        UBus.getInstance().post(new DialogClick(true, customDialog));
    }

    @Override
    public void onCancelClick() {
        UBus.getInstance().post(new DialogClick(false, customDialog));
    }

}
