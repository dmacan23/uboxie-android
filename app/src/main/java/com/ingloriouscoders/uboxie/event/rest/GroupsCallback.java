package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.data.controller.GroupController;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.network.response.GroupsResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 12/24/14.
 */
public class GroupsCallback implements Callback<GroupsResponse> {

    private static GroupsCallback groupsCallback;

    private GroupsCallback() {
    }

    public static GroupsCallback getInstance() {
        if (groupsCallback == null)
            groupsCallback = new GroupsCallback();
        return groupsCallback;
    }

    @Override
    public void success(GroupsResponse groupsResponse, Response response) {
        UBus.getInstance().post(GroupController.processResponse(groupsResponse));
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(new UboxieError(error));
    }

}
