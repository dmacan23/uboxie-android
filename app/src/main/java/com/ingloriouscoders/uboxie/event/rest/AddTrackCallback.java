package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.network.response.AddTrackResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 12/29/14.
 */
public class AddTrackCallback implements Callback<AddTrackResponse> {

    private static AddTrackCallback addTrackCallback;

    private AddTrackCallback() {

    }

    public static AddTrackCallback getInstance() {
        if (addTrackCallback == null)
            addTrackCallback = new AddTrackCallback();
        return addTrackCallback;
    }

    @Override
    public void success(AddTrackResponse addTrackResponse, Response response) {
        UBus.getInstance().post(addTrackResponse);
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(new UboxieError(error));
    }
}
