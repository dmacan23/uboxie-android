package com.ingloriouscoders.uboxie.event.socket;

import com.ingloriouscoders.uboxie.data.model.Group;

/**
 * Created by David on 25.1.2015..
 */
public class OnGroupChangedEvent {

    private Group newGroup;

    private Object[] args;

    public OnGroupChangedEvent(Object[] args) {
        this.args = args;
    }

    public Group getNewGroup() {
        return newGroup;
    }

    public Object[] getArgs() {
        return args;
    }
}
