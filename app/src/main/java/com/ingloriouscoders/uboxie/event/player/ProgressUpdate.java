package com.ingloriouscoders.uboxie.event.player;

/**
 * Created by dmacan on 12/24/14.
 */
public class ProgressUpdate {

    private long progress;

    public long getProgress() {
        return progress;
    }

    public void setProgress(long progress) {
        this.progress = progress;
    }
}
