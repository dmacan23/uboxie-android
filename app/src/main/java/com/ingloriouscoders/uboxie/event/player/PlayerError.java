package com.ingloriouscoders.uboxie.event.player;

import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;

/**
 * Created by dmacan on 12/25/14.
 */
public class PlayerError implements Runnable {
    @Override
    public void run() {
        synchronized (this) {
            UboxieError error = new UboxieError();
            error.setDescription("Player error");
            UBus.getInstance().post(error);
        }
    }
}
