package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.network.response.UserResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 1/25/15.
 */
public class UserResponseCallback implements Callback<UserResponse> {

    private static UserResponseCallback userResponseCallback;
    private String token;

    private UserResponseCallback() {

    }

    public static UserResponseCallback getInstance() {
        if (userResponseCallback == null)
            userResponseCallback = new UserResponseCallback();
        return userResponseCallback;
    }

    @Override
    public void success(UserResponse userResponse, Response response) {
        userResponse.getUser().setToken(token);
        UBus.getInstance().post(userResponse);
    }

    @Override
    public void failure(RetrofitError error) {
        UBus.getInstance().post(new UboxieError("User response error"));
    }

    public String getToken() {
        return token;
    }

    public UserResponseCallback setToken(String token) {
        this.token = token;
        return this;
    }
}
