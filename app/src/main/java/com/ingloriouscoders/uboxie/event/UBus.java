package com.ingloriouscoders.uboxie.event;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by dmacan on 12/24/14.
 * <p/>
 * This is Uboxie implementation of the Bus class used in Otto library. It can be used as a singleton for most use cases throughout the application. But if needed it can create another, non-singleton instance of itself within any thread
 * Using this class, users can register, unregister, post and handle events
 */
public class UBus extends Bus {

//    private static List<Object> subscribers;

    private static UBus bus;

    private UBus() {

    }

    private UBus(ThreadEnforcer enforcer) {
        super(enforcer);
    }

    /**
     * Standard singleton instantiation method
     *
     * @return UBus object
     */
    public static UBus getInstance() {
        if (bus == null) {
            bus = new UBus(ThreadEnforcer.MAIN);
//            subscribers = new ArrayList<>();
        }
        return bus;
    }

    /**
     * Returns new instance with chosen ThreadEnforcer
     *
     * @param enforcer Enforcer to use within any thread desired
     * @return New instance of UBus object with specified ThreadEnforcer
     */
    public static UBus newInstance(ThreadEnforcer enforcer) {
        return new UBus(enforcer);
    }

    public static void unregisterSafe(Object object) {
//        if (isObjectRegistered(object)) {
//            getInstance().unregister(object);
//            subscribers.remove(object);
//        }
        try {
            getInstance().unregister(object);
        } catch (Exception e) {
        }
    }

    public static void registerSafe(Object object) {
//        if (!isObjectRegistered(object)) {
//            ULog.d("Object: " + Uboxie.serialize(object));
        unregisterSafe(object);
        getInstance().register(object);
//            subscribers.add(object);
//        }
    }

    public static boolean isObjectRegistered(Object object) {
//        for (Object o : subscribers)
//            if (o == object)
//                return true;
        return false;
    }
}
