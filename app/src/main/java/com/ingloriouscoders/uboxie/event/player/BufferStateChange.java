package com.ingloriouscoders.uboxie.event.player;

import com.ingloriouscoders.uboxie.event.UBus;

/**
 * Created by dmacan on 12/25/14.
 */
public class BufferStateChange implements Runnable {

    private com.deezer.sdk.player.event.BufferState state;

    @Override
    public void run() {
        synchronized (this) {
            UBus.getInstance().post(state);
        }
    }

    public com.deezer.sdk.player.event.BufferState getState() {
        return state;
    }

    public void setState(com.deezer.sdk.player.event.BufferState state) {
        this.state = state;
    }
}
