package com.ingloriouscoders.uboxie.event.socket;

import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.util.Uboxie;

/**
 * Created by David on 25.1.2015..
 */
public class OnTrackAddedEvent {

    private Object[] args;

    private Track newTrack;

    public OnTrackAddedEvent(Object[] args) {
        this.args = args;
        if (args[0] != null)
            this.newTrack = Uboxie.deserialize(Uboxie.serializeToJSONObject(args[0]).get("nameValuePairs").getAsJsonObject().get("message").getAsJsonObject().get("nameValuePairs").getAsJsonObject().toString(), Track.class);
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Track getNewTrack() {
        return newTrack;
    }
}
