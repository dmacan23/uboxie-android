package com.ingloriouscoders.uboxie.event.player;

/**
 * Created by dmacan on 12/25/14.
 */
public class SeekStatus {

    private boolean isSeeking;

    public SeekStatus(boolean isSeeking) {
        this.isSeeking = isSeeking;
    }

    public boolean isSeeking() {
        return isSeeking;
    }

    public void setSeeking(boolean isSeeking) {
        this.isSeeking = isSeeking;
    }
}
