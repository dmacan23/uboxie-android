package com.ingloriouscoders.uboxie.event.player.action;

import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/28/14.
 */
public class PlayAction {

    private UboxiePlayer player;
    private Track track;
    private int offset;
    private int position;

    public PlayAction(UboxiePlayer player, Track track, int offset) {
        this.player = player;
        this.track = track;
        this.offset = offset;
    }

    public PlayAction(UboxiePlayer player, Track track, int offset, int position) {
        this.player = player;
        this.track = track;
        this.offset = offset;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public UboxiePlayer getPlayer() {
        return player;
    }

    public void setPlayer(UboxiePlayer player) {
        this.player = player;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
