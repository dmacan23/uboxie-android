package com.ingloriouscoders.uboxie.event.player;

import android.app.Activity;

import com.ingloriouscoders.uboxie.core.deezer.DeezerPlayer;

/**
 * Created by dmacan on 12/25/14.
 */
public class PlayerHandlerEvent {

    private Activity activity;
    private DeezerPlayer deezerPlayer;

    public PlayerHandlerEvent(Activity activity, DeezerPlayer deezerPlayer) {
        this.activity = activity;
        this.deezerPlayer = deezerPlayer;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public DeezerPlayer getDeezerPlayer() {
        return deezerPlayer;
    }

    public void setDeezerPlayer(DeezerPlayer deezerPlayer) {
        this.deezerPlayer = deezerPlayer;
    }
}
