package com.ingloriouscoders.uboxie.event.player;

/**
 * Created by dmacan on 12/24/14.
 */
public class SeekPosition {

    private double progressPercentage;
    private boolean seekable;
    private boolean isSeeking;

    public SeekPosition(double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }

    public SeekPosition(double progressPercentage, boolean seekable) {
        this.progressPercentage = progressPercentage;
        this.seekable = seekable;
    }

    public double getProgressPercentage() {
        return progressPercentage;
    }

    public void setProgressPercentage(double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }

    public boolean isSeekable() {
        return seekable;
    }

    public void setSeekable(boolean seekable) {
        this.seekable = seekable;
    }

    public boolean isSeeking() {
        return isSeeking;
    }

    public void setSeeking(boolean isSeeking) {
        this.isSeeking = isSeeking;
    }
}
