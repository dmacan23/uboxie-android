package com.ingloriouscoders.uboxie.event.player;

import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UboxieError;

/**
 * Created by dmacan on 12/25/14.
 */
public class BufferError implements Runnable {


    @Override
    public void run() {
        UboxieError error = new UboxieError();
        error.setDescription("Buffer error");
        UBus.getInstance().post(error);
    }
}
