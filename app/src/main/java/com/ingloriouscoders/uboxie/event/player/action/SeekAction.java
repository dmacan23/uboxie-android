package com.ingloriouscoders.uboxie.event.player.action;

import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/28/14.
 */
public class SeekAction {
    private UboxiePlayer player;
    private Track track;
    private long position;

    public SeekAction(UboxiePlayer player, Track track, long position) {
        this.player = player;
        this.track = track;
        this.position = position;
    }

    public UboxiePlayer getPlayer() {
        return player;
    }

    public void setPlayer(UboxiePlayer player) {
        this.player = player;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }
}
