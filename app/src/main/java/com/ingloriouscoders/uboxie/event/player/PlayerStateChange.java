package com.ingloriouscoders.uboxie.event.player;

import com.deezer.sdk.player.event.PlayerState;
import com.ingloriouscoders.uboxie.event.UBus;

/**
 * Created by dmacan on 12/25/14.
 */
public class PlayerStateChange implements Runnable {

    private PlayerState playerState;

    @Override
    public void run() {
        synchronized (this) {
            UBus.getInstance().post(playerState);
        }
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
}
