package com.ingloriouscoders.uboxie.event.other;

import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/27/14.
 */
public class TrackChosen {

    private Track track;

    public TrackChosen(Track track) {
        this.track = track;
    }

    public Track getTrack() {
        return track;
    }
}
