package com.ingloriouscoders.uboxie.event.click;

import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by dmacan on 12/26/14.
 */
public class DialogClick {

    private boolean confirmed;
    private CustomDialog dialog;

    public DialogClick(boolean confirmed, CustomDialog dialog) {
        this.confirmed = confirmed;
        this.dialog = dialog;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public CustomDialog getDialog() {
        return dialog;
    }

    public void setDialog(CustomDialog dialog) {
        this.dialog = dialog;
    }
}
