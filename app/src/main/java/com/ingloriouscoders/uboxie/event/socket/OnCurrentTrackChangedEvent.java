package com.ingloriouscoders.uboxie.event.socket;

import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by David on 25.1.2015..
 */
public class OnCurrentTrackChangedEvent {

    private Object[] args;
    private Track changedTrack;

    public OnCurrentTrackChangedEvent(Object[] args) {
        this.args = args;
    }

    public Object[] getArgs() {
        return args;
    }

    public Track getChangedTrack() {
        return changedTrack;
    }

}
