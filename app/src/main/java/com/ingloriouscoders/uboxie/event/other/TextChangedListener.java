package com.ingloriouscoders.uboxie.event.other;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.event.UBus;

/**
 * Created by dmacan on 12/27/14.
 */
public class TextChangedListener implements TextWatcher, TextView.OnEditorActionListener {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        ULog.d("Before text changed");
//        UBus.getInstance().post(new TextChanged(s, start, count, after, TextChanged.Status.BEFORE));
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        UBus.getInstance().post(new TextChanged(s.toString()));
    }

    @Override
    public void afterTextChanged(Editable s) {
//        ULog.d("After text changed");
//        UBus.getInstance().post(new TextChanged(s, TextChanged.Status.AFTER));
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            UBus.getInstance().post(new TextChanged(v.getText().toString(), true));
            return true;
        }
        return false;
    }
}
