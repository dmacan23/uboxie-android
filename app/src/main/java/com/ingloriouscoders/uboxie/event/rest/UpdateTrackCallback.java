package com.ingloriouscoders.uboxie.event.rest;

import com.ingloriouscoders.uboxie.network.response.UpdateTrackResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmacan on 1/27/15.
 */
public class UpdateTrackCallback implements Callback<UpdateTrackResponse> {

    private static UpdateTrackCallback updateTrackCallback;

    public static UpdateTrackCallback getInstance() {
        if (updateTrackCallback == null)
            updateTrackCallback = new UpdateTrackCallback();
        return updateTrackCallback;
    }

    private UpdateTrackCallback() {

    }

    @Override
    public void success(UpdateTrackResponse updateTrackResponse, Response response) {
    }

    @Override
    public void failure(RetrofitError error) {
    }
}
