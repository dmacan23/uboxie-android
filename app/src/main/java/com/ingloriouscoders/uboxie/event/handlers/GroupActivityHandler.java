package com.ingloriouscoders.uboxie.event.handlers;

import android.support.v4.app.Fragment;

import com.ingloriouscoders.uboxie.GroupActivity;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.TrackChosen;
import com.ingloriouscoders.uboxie.event.player.ProgressUpdate;
import com.ingloriouscoders.uboxie.event.socket.OnTrackAddedEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by David on 3.3.2015..
 */
public class GroupActivityHandler {

    private GroupActivity activity;

    public GroupActivityHandler(GroupActivity activity) {
        this.activity = activity;
    }

    public GroupActivityHandler start() {
        UBus.unregisterSafe(this);
        UBus.registerSafe(this);
        return this;
    }

    public GroupActivityHandler stop() {
        UBus.unregisterSafe(this);
        return this;
    }

    public GroupActivity getActivity() {
        return activity;
    }

    public void setActivity(GroupActivity activity) {
        this.activity = activity;
    }

    @Subscribe
    public void onGroupLoadedEvent(Group group) {
        activity.onGroupLoaded(group);
    }

    @Subscribe
    public void onFragmentReadyEvent(Fragment fragment) {
        activity.onFragmentReady(fragment);
    }

    @Subscribe
    public void onProgressUpdateEvent(ProgressUpdate progressUpdate) {
        activity.onProgressUpdate(progressUpdate);
    }

    @Subscribe
    public void onTrackChosenEvent(TrackChosen trackChosen) {
        activity.onTrackChosen(trackChosen);
    }

    @Subscribe
    public void onTrackAddedEvent(OnTrackAddedEvent trackAddedEvent) {
        activity.onTrackAdded(trackAddedEvent);
    }


}
