package com.ingloriouscoders.uboxie.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.UboxieModel;

/**
 * Created by David on 14.12.2014..
 */
public class GroupsResponse extends UboxieModel {

    @Expose
    @SerializedName("message")
    private Group[] groups;

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }
}
