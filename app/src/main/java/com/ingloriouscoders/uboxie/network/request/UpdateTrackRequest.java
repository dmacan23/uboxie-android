package com.ingloriouscoders.uboxie.network.request;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by dmacan on 1/26/15.
 */
public class UpdateTrackRequest {

    private String trackId;
    private String startTime;

    public UpdateTrackRequest(String trackId) {
        this.trackId = trackId;
        this.startTime = String.valueOf(new DateTime(DateTimeZone.UTC).getMillis());
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
