package com.ingloriouscoders.uboxie.network.request;

import com.google.gson.annotations.Expose;
import com.lightandroid.type.LightData;

/**
 * Created by dmacan on 5.11.2014..
 */
public class TrackQueryRequest extends LightData {

    @Expose
    private String query;

    @Expose
    String mask;

    public TrackQueryRequest(String query, String mask) {
        this.query = query;
        this.mask = mask;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }
}
