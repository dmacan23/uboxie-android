package com.ingloriouscoders.uboxie.network;

import com.ingloriouscoders.uboxie.network.request.UpdateTrackRequest;
import com.ingloriouscoders.uboxie.network.response.AddTrackResponse;
import com.ingloriouscoders.uboxie.network.response.GroupCreatedResponse;
import com.ingloriouscoders.uboxie.network.response.GroupResponse;
import com.ingloriouscoders.uboxie.network.response.GroupsResponse;
import com.ingloriouscoders.uboxie.network.response.TrackQueryResponse;
import com.ingloriouscoders.uboxie.network.response.UpdateTrackResponse;
import com.ingloriouscoders.uboxie.network.response.UserResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by dmacan on 5.11.2014..
 */
public interface UboxieAPI {

    public static final String ENDPOINT = "http://uboxie.me/api/1";
    public static final String TAG = "RetroUboxie";

    @FormUrlEncoded
    @POST("/getTracks")
    void trackQuery(@Field("query") String query, @Field("limit") int limit, @Field("token") String token, Callback<TrackQueryResponse> response);

    @GET("/groups/find/")
    void getGroups(@Query("startPosition") int startPosition, @Query("endPosition") int endPosition, Callback<GroupsResponse> response);

    @FormUrlEncoded
    @POST("/groups/new")
    void createGroup(@Field("groupName") String groupName, @Field("token") String token, Callback<GroupCreatedResponse> callback);

    @GET("/groups/find/{id}")
    void getGroup(@Path("id") String groupId, Callback<GroupResponse> callback);

    @FormUrlEncoded
    @POST("/group/{id}/state/preserve")
    void addTrackToGroup(@Path("id") String groupId, @Field("duration") int duration, @Field("name") String title, @Field("key") long key, @Field("token") String token, @Field("info") String info, @Field("artist") String artist, @Field("icon") String icon, Callback<AddTrackResponse> callback);

    @GET("/user/current/{token}")
    void getUserInfo(@Path("token") String token, Callback<UserResponse> callback);

    @POST("/group/{id}/state/current")
    void updateCurrentTrack(@Path("id") String groupId, @Body UpdateTrackRequest track, Callback<UpdateTrackResponse> callback);

    @FormUrlEncoded
    @POST("/group/{id}/state/current")
    void updateCurrentTrack(@Path("id") String groupId, @Field("trackId") String trackId, @Field("startTime") String startTime, Callback<UpdateTrackResponse> callback);

}
