package com.ingloriouscoders.uboxie.network.response;

import com.google.gson.annotations.SerializedName;
import com.ingloriouscoders.uboxie.data.model.Group;

/**
 * Created by dmacan on 12/28/14.
 */
public class GroupResponse {

    private String status;
    @SerializedName("message")
    private Group[] groups;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }
}
