package com.ingloriouscoders.uboxie.network.response;

import com.google.gson.annotations.SerializedName;
import com.ingloriouscoders.uboxie.data.model.User;

/**
 * Created by dmacan on 1/25/15.
 */
public class UserResponse {
    private String status;
    @SerializedName("message")
    private User user;

    public UserResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
