package com.ingloriouscoders.uboxie.network.response;

import com.ingloriouscoders.uboxie.data.model.Group;

/**
 * Created by dmacan on 1/27/15.
 */
public class GroupCreatedResponse {

    private String status;
    private Group group;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
