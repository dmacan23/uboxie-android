package com.ingloriouscoders.uboxie.network;

import android.app.Activity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.socket.OnConnectEvent;
import com.ingloriouscoders.uboxie.event.socket.OnGroupAddedEvent;
import com.ingloriouscoders.uboxie.event.socket.OnTrackAddedEvent;
import com.ingloriouscoders.uboxie.network.response.AddTrackResponse;
import com.ingloriouscoders.uboxie.util.Uboxie;

import java.net.URISyntaxException;

/**
 * Created by dmacan on 12/24/14.
 * <p/>
 * This track is used for Socket.IO implementation for real-time communication between the client and server
 * TODO add socket.io implementation
 */
public class USocket {

    private static final String ENDPOINT = "http://uboxie.me";
    private static USocket uSocket;
    private Socket socket;
    private boolean isConnected;

    private USocket() {
        try {
            IO.Options opts = new IO.Options();
            socket = IO.socket(ENDPOINT);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static USocket getInstance() {
        if (uSocket == null)
            uSocket = new USocket();
        return uSocket;
    }

    public void connect() {
        socket.connect();
    }

    public void listenOnConnect() {
        socket.on(Event.CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                isConnected = true;
                UBus.getInstance().post(new OnConnectEvent(args));
            }
        });
    }

    public void listenOnTrackAdded(final Activity activity) {
        socket.on(Event.TRACK_ADDED, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UBus.getInstance().post(new OnTrackAddedEvent(args));
                    }
                });
            }
        });
    }

    public void listenOnGroupAdded(final Activity activity) {
        socket.on(Event.GROUP_ADDED, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UBus.getInstance().post(new OnGroupAddedEvent(args));
                    }
                });
            }
        });
    }

    public void emitGroupJoin(Group group, User user) {
        socket.emit(Event.EMIT_GROUP_JOIN, group.getId(), user.getName());
    }

    public void emitGroupCreate(Group group) {
        socket.emit(Event.GROUP_CREATE, Uboxie.serialize(group));
    }

    public void emitAddTrack(AddTrackResponse addTrackResponse) {
        socket.emit(Event.TRACK_ADD, Uboxie.serialize(addTrackResponse));
    }


    public static class Event {
        public static final String CONNECT = "connection";
        public static final String TRACK_ADDED = "updateGroupQueue";
        public static final String GROUP_ADDED = "newGroup";
        public static final String GROUP_CREATE = "addGroup";
        public static final String TRACK_ADD = "songAdded";
        public static final String CURRENT_TRACK_CHANGED = "currentTrackChanged";
        public static final String EMIT_GROUP_JOIN = "joinGroup";
    }

}