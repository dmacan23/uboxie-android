package com.ingloriouscoders.uboxie.network.response;

import com.ingloriouscoders.uboxie.data.model.Track;
import com.lightandroid.type.LightData;

/**
 * Created by dmacan on 5.11.2014..
 */
public class TrackQueryResponse extends LightData {

    private String status;
    private Track[] result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Track[] getResult() {
        return result;
    }

    public void setResult(Track[] result) {
        this.result = result;
    }
}
