package com.ingloriouscoders.uboxie.network.response;

import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by dmacan on 12/29/14.
 */
public class AddTrackResponse {

    private String status;
    private Track message;

    public AddTrackResponse() {
    }

    public AddTrackResponse(Track message) {
        this.status = "true";
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Track getMessage() {
        return message;
    }

    public void setMessage(Track message) {
        this.message = message;
    }
}
