package com.ingloriouscoders.uboxie;

import com.ingloriouscoders.uboxie.fragment.MainFragment;
import com.ingloriouscoders.uboxie.network.USocket;
import com.lightandroid.navigation.activity.LightActivity;

/**
 * Created by David on 6.10.2014..
 */
public class MainActivity extends LightActivity {

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main() {
        USocket.getInstance().connect();
        setupFragment(R.id.container, new MainFragment());
    }
}
