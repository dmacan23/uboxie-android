package com.ingloriouscoders.uboxie;

import com.ingloriouscoders.uboxie.fragment.AddTrackFragment;
import com.lightandroid.navigation.activity.LightActivity;

/**
 * Created by dmacan on 12/27/14.
 */
public class AddTrackActivity extends LightActivity {

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main() {
        setupFragment(R.id.container, new AddTrackFragment());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
