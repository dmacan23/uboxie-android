package com.ingloriouscoders.uboxie.core.deezer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.deezer.sdk.network.connect.DeezerConnect;
import com.deezer.sdk.network.connect.SessionStore;
import com.deezer.sdk.network.connect.event.DialogListener;
import com.deezer.sdk.network.request.event.RequestListener;
import com.ingloriouscoders.uboxie.core.musicapi.UboxieUserAuthenticator;
import com.ingloriouscoders.uboxie.data.model.User;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UserAuthentication;
import com.ingloriouscoders.uboxie.event.rest.UserResponseCallback;
import com.ingloriouscoders.uboxie.util.C;
import com.ingloriouscoders.uboxie.util.Uboxie;

/**
 * Created by David on 12.12.2014..
 */
public class DeezerUserManager implements UboxieUserAuthenticator, DialogListener, RequestListener {

    private DeezerConnect deezerConnect;
    private Activity activity;
    private SessionStore sessionStore;

    public DeezerUserManager(Activity activity) {
        this.activity = activity;
        this.deezerConnect = new DeezerConnect(activity, Deezer.APP_ID);
        this.sessionStore = new SessionStore();
    }

    @Override
    public void authenticateUser(Context context) {
        deezerConnect.authorize(activity, Deezer.PERMISSIONS, this);
    }

    @Override
    public boolean isUserLoggedIn(Context context) {
//        User user = User.load(context);
//        boolean isLogged = (this.sessionStore.restore(deezerConnect, activity) && user != null && user.getName() != null && !user.getName().equals(""));
//        if (!isLogged)
//            sessionStore.clear(context);
//        return isLogged;
        return this.sessionStore.restore(deezerConnect, activity);
    }

    @Override
    public void storeUser(Context context, User user) {
        sessionStore.save(deezerConnect, context);
        user.store(context);
    }

    @Override
    public User loadUser(Context context) {
        if (isUserLoggedIn(context))
            return User.load(context);
        return null;
    }

    @Override
    public void clearUserData(Context context) {
        if (deezerConnect != null && activity != null)
            deezerConnect.logout(activity);
        sessionStore.clear(context);
        Uboxie.loadEditor(context, C.Key.PREFERENCES).clear().commit();
    }

    @Override
    public void onComplete(Bundle values) {
        String token = values.getString(Deezer.KEY_USER_TOKEN);
        String name = values.getString(Deezer.KEY_USER_NAME);
        User user = new User();
        user.setToken(token);
        user.setAuthenticated(true);
        user.setName(name);
        storeUser(activity, user);
        UBus.getInstance().post(new UserAuthentication(user, true));
    }

    @Override
    public void onCancel() {
        UBus.getInstance().post(new UserAuthentication(null, false));
    }

    @Override
    public void onException(Exception e) {
        UBus.getInstance().post(new UserAuthentication(null, false));
    }

    @Override
    public void onComplete(String s, Object o) {
        UBus.getInstance().post(new UserAuthentication(null, true));
    }

    @Override
    public void onException(Exception e, Object o) {
        UBus.getInstance().post(new UserAuthentication(null, false));
    }

    public DeezerConnect getDeezerConnect() {
        return deezerConnect;
    }

    @Override
    public void getUserData(String token) {
        Uboxie.API().getUserInfo(token, UserResponseCallback.getInstance().setToken(token));
    }
}
