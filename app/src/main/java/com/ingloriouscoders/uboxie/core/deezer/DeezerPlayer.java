package com.ingloriouscoders.uboxie.core.deezer;

import android.app.Activity;

import com.deezer.sdk.network.connect.DeezerConnect;
import com.deezer.sdk.network.connect.SessionStore;
import com.deezer.sdk.player.TrackPlayer;
import com.deezer.sdk.player.event.PlayerWrapperListener;
import com.deezer.sdk.player.networkcheck.WifiAndMobileNetworkStateChecker;
import com.ingloriouscoders.uboxie.core.musicapi.UboxiePlayer;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.player.SeekPosition;
import com.ingloriouscoders.uboxie.event.player.SeekStatus;
import com.ingloriouscoders.uboxie.event.player.action.PauseAction;
import com.ingloriouscoders.uboxie.event.player.action.ResumeAction;
import com.ingloriouscoders.uboxie.event.player.action.SeekAction;
import com.ingloriouscoders.uboxie.event.player.action.StopAction;
import com.ingloriouscoders.uboxie.event.player.action.TrackEndedAction;
import com.ingloriouscoders.uboxie.util.DeviceUtil;
import com.squareup.otto.Subscribe;

/**
 * Created by David on 14.12.2014..
 */
public class DeezerPlayer implements UboxiePlayer, PlayerWrapperListener {

    private static DeezerPlayer deezerPlayer;
    private TrackPlayer trackPlayer;
    private DeezerConnect deezerConnect;
    private Activity activity;
    private Track currentlyPlaying;
    private boolean isPlaying = false;
    private boolean isSeeking = false;
    private boolean ready;
    private DeezerPlayerHandler deezerPlayerHandler;
    private int seekableValidity;

    private DeezerPlayer(DeezerConnect deezerConnect, Activity activity) {
        UBus.getInstance().register(this);
        this.deezerConnect = deezerConnect;
        this.activity = activity;
    }

    /**
     * Standard singleton implementation
     *
     * @param deezerConnect DeezerConnect object required to instantiate player
     * @param activity      Activity which uses the player
     * @return DeezerPlayer object
     */
    public static DeezerPlayer getInstance(DeezerConnect deezerConnect, Activity activity) {
        if (deezerPlayer == null) {
            deezerPlayer = new DeezerPlayer(deezerConnect, activity);
        }
        return deezerPlayer;
    }

    /**
     * Initializes the player by setting some listeners and adding a player handler
     */
    private void initPlayer() {
        if (this.trackPlayer == null) {
            try {
                this.trackPlayer = new TrackPlayer(activity.getApplication(), deezerConnect, new WifiAndMobileNetworkStateChecker());
                this.deezerPlayerHandler = new DeezerPlayerHandler(this, this.activity);
                this.trackPlayer.addPlayerListener(this);
                this.trackPlayer.addOnPlayerProgressListener(this.deezerPlayerHandler);
                this.trackPlayer.addOnBufferProgressListener(this.deezerPlayerHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void play(Track track, int offset) {
        if (!track.isFinished() || offset == 0) {
            this.currentlyPlaying = track;
            this.prepare();
            this.trackPlayer.playTrack(track.getKey());
            if (offset > 0)
                seek(offset);
            this.isPlaying = true;
        }
    }

    @Override
    public void pause() {
        DeviceUtil.toggleMusicMute(activity, true);
        trackPlayer.pause();
        isPlaying = false;
        UBus.getInstance().post(new PauseAction(currentlyPlaying, this, trackPlayer.getPosition()));
    }

    @Override
    public void stop() {
        if (this.trackPlayer != null) {
            this.trackPlayer.stop();
            this.isPlaying = false;
            UBus.getInstance().post(new StopAction(currentlyPlaying, this, trackPlayer.getPosition()));
        }
    }

    @Override
    public void resume() {
        this.trackPlayer.play();
        this.isPlaying = true;
        UBus.getInstance().post(new ResumeAction(currentlyPlaying, this, trackPlayer.getPosition()));
    }

    @Override
    public void create() {
        initPlayer();
    }

    @Override
    public void destroy() {
        if (this.trackPlayer != null) {
            this.trackPlayer.stop();
            this.trackPlayer.release();
        }
    }

    @Override
    public boolean isPlaying() {
        return this.isPlaying;
    }

    @Override
    public boolean seek(int seconds) {
        this.isSeeking = true;
        pause();
        return this.currentlyPlaying.getDuration() > seconds;
    }

    @Override
    public boolean playNext() {
        return false;
    }

    @Override
    public boolean prepare() {
        if (!this.ready)
            this.ready = new SessionStore().restore(deezerConnect, activity);
        if (this.trackPlayer == null)
            initPlayer();
        return ready;
    }

    @Subscribe
    public void onSeekable(SeekPosition seekPosition) {
        seek(this.currentlyPlaying.getStartTimeOffset(), false);
    }

    /**
     * Seeks track by given offset
     *
     * @param offset Offset to seek to
     * @param seek   If true, seek and vice-versa
     */
    private void seek(long offset, boolean seek) {
        this.isSeeking = seek;
        this.trackPlayer.seek(offset);
        DeviceUtil.toggleMusicMute(activity, false);
        UBus.getInstance().post(new SeekStatus(seek));
        UBus.getInstance().post(new SeekAction(this, currentlyPlaying, offset));
    }

    /**
     * Checks if the object is seekable
     *
     * @param progressPercentage
     * @param seekPosition
     * @param duration
     * @return True if object is seekable and vice-versa
     */
    private boolean isSeekable(double progressPercentage, long seekPosition, long duration) {
        if (duration <= 0)
            return false;
        double realProgress = progressPercentage / 100;
        double requiredProgress = (seekPosition / duration);
        requiredProgress /= 1000;
        boolean progressSatisfied = (realProgress) > (requiredProgress);
        if (progressSatisfied)
            seekableValidity += 1;
        if (seekableValidity > 3) {
            seekableValidity = 0;
            return true;
        }
        return false;
    }

    /**
     * Checks if the object is seekable
     *
     * @param progressPercentage
     * @return True if object is seekable and vice-versa
     */
    public boolean isSeekable(double progressPercentage) {
        return isSeekable(progressPercentage, this.currentlyPlaying.getStartTimeOffset(), this.currentlyPlaying.getDuration());
    }

    @Override
    public void onAllTracksEnded() {
        // No implementation needed
    }

    @Override
    public void onPlayTrack(com.deezer.sdk.model.Track track) {
        this.currentlyPlaying.setDuration(track.getDuration());
        UBus.getInstance().post(this.currentlyPlaying);
    }

    @Override
    public void onTrackEnded(com.deezer.sdk.model.Track track) {
        this.isPlaying = false;
        UBus.getInstance().post(new TrackEndedAction(currentlyPlaying, this));
    }

    @Override
    public void onRequestException(Exception e, Object o) {

    }

    @Override
    public boolean isSeeking() {
        return isSeeking;
    }

    /**
     * Sets seeking flag
     *
     * @param isSeeking flag value to be set
     */
    public void setSeeking(boolean isSeeking) {
        this.isSeeking = isSeeking;
    }
}
