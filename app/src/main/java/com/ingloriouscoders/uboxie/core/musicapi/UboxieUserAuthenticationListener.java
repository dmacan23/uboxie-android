package com.ingloriouscoders.uboxie.core.musicapi;

/**
 * Created by David on 12.12.2014..
 */
public interface UboxieUserAuthenticationListener {

    public void onAuthentication(boolean successful);

}
