package com.ingloriouscoders.uboxie.core.deezer;

import com.deezer.sdk.model.Track;
import com.deezer.sdk.network.request.event.JsonRequestListener;
import com.deezer.sdk.player.TrackPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 12.12.2014..
 */
public class TrackRequestListener extends JsonRequestListener {

    private TrackPlayer trackPlayer;

    public TrackRequestListener(TrackPlayer trackPlayer) {
        this.trackPlayer = trackPlayer;
    }

    @Override
    public void onResult(Object o, Object o2) {
        List<Track> tracks = new ArrayList<>();
        tracks.addAll((List<Track>) o);
        trackPlayer.playTrack(tracks.get(0).getId());
    }

    @Override
    public void onUnparsedResult(String s, Object o) {

    }

    @Override
    public void onException(Exception e, Object o) {

    }
}
