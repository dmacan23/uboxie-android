package com.ingloriouscoders.uboxie.core.musicapi;

import android.app.Activity;
import android.content.Context;

import com.ingloriouscoders.uboxie.data.model.User;

/**
 * Created by David on 12.12.2014..
 */
public interface UboxieUserAuthenticator {

    public void authenticateUser(Context context);

    public boolean isUserLoggedIn(Context context);

    public void storeUser(Context context, User user);

    public User loadUser(Context context);

    public void clearUserData(Context context);

    public void getUserData(String token);

}
