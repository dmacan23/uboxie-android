package com.ingloriouscoders.uboxie.core.musicapi;

import com.ingloriouscoders.uboxie.data.model.Track;

/**
 * Created by David on 14.12.2014..
 */
public interface UboxiePlayer {

    /**
     * Plays given track
     *
     * @param track Track to be played
     */
    public void play(Track track, int offset);

    /**
     * Pauses playing
     */
    public void pause();

    /**
     * Stops playing (and buffering)
     */
    public void stop();

    /**
     * Resumes playing
     */
    public void resume();

    /**
     * Initializes player
     */
    public void create();

    /**
     * Stops player and deinitializes it
     */
    public void destroy();

    /**
     * Checks if the player is currently playing tracks
     *
     * @return true if the player is playing, false if it isn't
     */
    public boolean isPlaying();

    /**
     * Seeks the track to given second
     *
     * @param seconds Seconds to seek to
     */
    public boolean seek(int seconds);

    /**
     * Plays next track in the playlist (if there is such)
     */
    public boolean playNext();

    /**
     * Prepares player for playing
     *
     * @return True if preparation successful and vice-versa
     */
    public boolean prepare();

    /**
     * Checks if the player is currently seeking track
     *
     * @return True/false state of the condition
     */
    public boolean isSeeking();
}
