package com.ingloriouscoders.uboxie.core.deezer;

import android.app.Activity;

import com.deezer.sdk.player.event.BufferState;
import com.deezer.sdk.player.event.OnBufferErrorListener;
import com.deezer.sdk.player.event.OnBufferProgressListener;
import com.deezer.sdk.player.event.OnBufferStateChangeListener;
import com.deezer.sdk.player.event.OnPlayerErrorListener;
import com.deezer.sdk.player.event.OnPlayerProgressListener;
import com.deezer.sdk.player.event.OnPlayerStateChangeListener;
import com.deezer.sdk.player.event.PlayerState;
import com.ingloriouscoders.uboxie.event.player.BufferError;
import com.ingloriouscoders.uboxie.event.player.BufferProgress;
import com.ingloriouscoders.uboxie.event.player.BufferStateChange;
import com.ingloriouscoders.uboxie.event.player.PlayerError;
import com.ingloriouscoders.uboxie.event.player.PlayerProgress;
import com.ingloriouscoders.uboxie.event.player.PlayerStateChange;

/**
 * Created by dmacan on 12/24/14.
 */
public class DeezerPlayerHandler implements OnPlayerProgressListener, OnBufferProgressListener, OnPlayerStateChangeListener, OnPlayerErrorListener, OnBufferStateChangeListener, OnBufferErrorListener {

    private Activity activity;
    private PlayerProgress playerProgress;
    private BufferError bufferError;
    private BufferProgress bufferProgress;
    private PlayerError playerError;
    private BufferStateChange bufferStateChange;
    private PlayerStateChange playerStateChange;

    public DeezerPlayerHandler(DeezerPlayer deezerPlayer, Activity activity) {
        this.activity = activity;
        this.playerProgress = new PlayerProgress(activity, deezerPlayer);
        this.bufferError = new BufferError();
        this.bufferProgress = new BufferProgress(activity, deezerPlayer);
        this.playerError = new PlayerError();
        this.playerStateChange = new PlayerStateChange();
        this.bufferStateChange = new BufferStateChange();
    }

    @Override
    public void onBufferError(Exception e, double v) {
        activity.runOnUiThread(bufferError);
    }

    @Override
    public void onBufferProgress(final double v) {
        synchronized (this) {
            bufferProgress.setProgressPercentage(v);
            activity.runOnUiThread(bufferProgress);
        }
    }

    @Override
    public void onBufferStateChange(final BufferState bufferState, double v) {
        synchronized (this) {
            bufferStateChange.setState(bufferState);
            activity.runOnUiThread(bufferStateChange);
        }
    }

    @Override
    public void onPlayerError(Exception e, long l) {
        synchronized (this) {
            activity.runOnUiThread(playerError);
        }
    }

    @Override
    public void onPlayerProgress(final long l) {
        synchronized (this) {
            playerProgress.setProgress(l);
            activity.runOnUiThread(playerProgress);
        }
    }

    @Override
    public void onPlayerStateChange(final PlayerState playerState, long l) {
        synchronized (this) {
            playerStateChange.setPlayerState(playerState);
            activity.runOnUiThread(playerStateChange);
        }
    }

}
