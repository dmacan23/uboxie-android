package com.ingloriouscoders.uboxie.fragment;

import com.ingloriouscoders.uboxie.event.other.UserAuthentication;

/**
 * Created by David on 14.12.2014..
 */
public interface LoginFragment {

    /**
     * Called after the authentication
     *
     * @param userAuthentication Object containing the authentication feedback data
     */
    public void onAuthentication(UserAuthentication userAuthentication);

}
