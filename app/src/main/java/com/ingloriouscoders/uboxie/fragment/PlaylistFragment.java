package com.ingloriouscoders.uboxie.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ingloriouscoders.uboxie.GroupActivity;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.controller.PlayController;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.data.presenter.TrackPresenter;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.TrackChosen;
import com.ingloriouscoders.uboxie.event.player.action.PlayAction;
import com.ingloriouscoders.uboxie.ui.LightRecyclerAdapter2;
import com.lightandroid.type.LightIconDrawable;
import com.melnykov.fab.FloatingActionButton;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 7.12.2014..
 * <p/>
 * Fragment displaying the current group's playlist
 */
public class PlaylistFragment extends BaseFragment {

    public static final String TAG = "com.uboxie.fragment.Playlist";

    @InjectView(R.id.listSongs)
    RecyclerView listSongs;
    @InjectView(R.id.fab)
    FloatingActionButton fab;

    private GroupActivity activity;
    private LightRecyclerAdapter2 playlistAdapter;
    private PlayController playController;
    private boolean ready = false;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_playlist;
    }

    @Override
    public void main() {
        UBus.registerSafe(this);
        this.activity = (GroupActivity) getBaseActivity();
        this.playController = activity.getPlayController();
        if (this.playlistAdapter == null)
            this.playlistAdapter = new LightRecyclerAdapter2();
        this.listSongs.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        this.listSongs.setAdapter(playlistAdapter);
        fab.attachToRecyclerView(listSongs);
        fab.setImageDrawable(new LightIconDrawable(getBaseActivity(), getString(R.string.mic_add)).colorRes(R.color.txt_white).sizeDp(24));
        if (!ready) {
            UBus.getInstance().post(this);
            ready = true;
        }
    }

    public void display(List<Track> tracks, int active) {
        if (playlistAdapter == null)
            playlistAdapter = new LightRecyclerAdapter2();
        playlistAdapter.clear();
        for (int i = 0; i < tracks.size(); i++) {
            TrackPresenter presenter = new TrackPresenter(tracks.get(i), true);
            presenter.setActive(i == active);
            playlistAdapter.addPresenter(presenter);
        }
    }

    /**
     * Marks track selected for given position and the one before it is marked non-selected
     *
     * @param position Position of the track to select
     */
    public void selectTrack(int position) {
        listSongs.scrollToPosition(position);
        int previous = position;
        if (previous > 0)
            previous -= 1;
        // Crash
        ((TrackPresenter) playlistAdapter.getPresenter(previous)).setActive(false);
        ((TrackPresenter) playlistAdapter.getPresenter(position)).setActive(true);
    }

    /**
     * Triggers after the user clicks on FloatingActionButton to add new track
     */
    @OnClick(R.id.fab)
    public void addNewTrack() {
        activity.addTracks();
        activity.getMenuDrawer().closeMenu();
    }

    /**
     * Called after track is played
     *
     * @param playAction Action containing information about track being played and player playing it
     */
    @Subscribe
    public void onTrackPlaying(PlayAction playAction) {
        selectTrack(playAction.getPosition());
    }

    /**
     * Triggered after the track has been chosen within the AddTrackFragment
     *
     * @param action Object containing data about track chosen action
     */
    @Subscribe
    public void onTrackChosen(TrackChosen action) {
//        playlistAdapter.addPresenter(new TrackPresenter(action.getTrack(), !action.getTrack().isFinishedPlaying()));
    }

    public void addTrack(Track track) {
        if (playlistAdapter == null)
            playlistAdapter = new LightRecyclerAdapter2();
        playlistAdapter.addPresenter(new TrackPresenter(track, !track.isFinishedPlaying()));
    }

    public List<TrackPresenter> getPlaylist() {
        if (this.playlistAdapter != null)
            return (List<TrackPresenter>) (List<?>) this.playlistAdapter.getPresenters();
        return null;
    }
}
