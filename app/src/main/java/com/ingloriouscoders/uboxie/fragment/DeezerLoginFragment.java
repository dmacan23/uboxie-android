package com.ingloriouscoders.uboxie.fragment;

import android.content.Intent;

import com.ingloriouscoders.uboxie.MainActivity;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.core.deezer.DeezerUserManager;
import com.ingloriouscoders.uboxie.core.musicapi.UboxieUserAuthenticator;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.UserAuthentication;
import com.ingloriouscoders.uboxie.network.response.UserResponse;
import com.squareup.otto.Subscribe;

/**
 * Created by David on 14.12.2014..
 * <p/>
 * This fragment is used for login with Deezer account using Deezer login methods
 */
public class DeezerLoginFragment extends BaseFragment implements LoginFragment {

    private UboxieUserAuthenticator uboxieUserAuthenticator;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void main() {
        UBus.getInstance().register(this);
        uboxieUserAuthenticator = new DeezerUserManager(getActivity());
        if (uboxieUserAuthenticator.isUserLoggedIn(getActivity()))
            moveOn();
        else {
            uboxieUserAuthenticator.authenticateUser(getActivity());
            showProgress(getString(R.string.lbl_progress_authenticating));
        }
    }

    /**
     * Starts the next activity and finishes this one
     */
    private void moveOn() {
        this.startActivity(new Intent(getActivity(), MainActivity.class));
        this.getLightActivity().finish();
    }

    /**
     * Triggered after the user has been authenticated
     * Dismisses the progress display.
     * If successful, continues to the next activity,
     * If not successful, displays the error message
     *
     * @param authentication Object containing data about user authentication
     */
    @Subscribe
    @Override
    public void onAuthentication(UserAuthentication authentication) {
        if (authentication.isSuccess())
            uboxieUserAuthenticator.getUserData(authentication.getUser().getToken());
        else
            showMessage(getString(R.string.error_authentication));
    }

    @Subscribe
    public void onUserDataRetrieved(UserResponse userResponse) {
        userResponse.getUser().store(getLightActivity());
        dismissProgress();
        moveOn();
    }

}
