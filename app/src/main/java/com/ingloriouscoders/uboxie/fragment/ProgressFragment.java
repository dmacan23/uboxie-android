package com.ingloriouscoders.uboxie.fragment;

import android.app.Activity;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.R;
import com.lightandroid.navigation.fragment.LightFragment;

import butterknife.InjectView;

/**
 * Created by dmacan on 1/2/15.
 * <p/>
 * This is a simple fragment used only to display progress
 */
public class ProgressFragment extends LightFragment {

    public static final String TAG = "com.ingloriouscoders.uboxie.tag.ProgressFragment";

    @InjectView(R.id.txtProgressMessage)
    TextView txtMessage;

    private String message;

    @Override
    public int provideLayoutRes() {
        return R.layout.layout_progress;
    }

    @Override
    public void main() {
        txtMessage.setText(this.message);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (txtMessage != null)
            txtMessage.setText(this.message);
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
