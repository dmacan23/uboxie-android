package com.ingloriouscoders.uboxie.fragment;

import com.ingloriouscoders.uboxie.data.model.Group;

/**
 * Created by dmacan on 12/24/14.
 */
public interface GroupPlayerFragment {

    /**
     * Displays the given group within the fragment
     *
     * @param group Group to be displayed
     */
    public void display(Group group);

}
