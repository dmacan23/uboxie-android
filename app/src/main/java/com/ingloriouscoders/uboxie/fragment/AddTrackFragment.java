package com.ingloriouscoders.uboxie.fragment;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.ingloriouscoders.uboxie.GroupActivity;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.controller.TrackController;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.data.presenter.TrackPresenter;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.TextChanged;
import com.ingloriouscoders.uboxie.event.other.TextChangedListener;
import com.ingloriouscoders.uboxie.event.other.TrackChosen;
import com.ingloriouscoders.uboxie.network.USocket;
import com.ingloriouscoders.uboxie.network.response.AddTrackResponse;
import com.ingloriouscoders.uboxie.network.response.TrackQueryResponse;
import com.ingloriouscoders.uboxie.ui.LightRecyclerAdapter2;
import com.ingloriouscoders.uboxie.util.DeviceUtil;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;

/**
 * Created by dmacan on 12/27/14.
 * <p/>
 * This fragment is used for adding more tracks to the playlist. User can write track or artist name in given EditText and then receives search results from web API.
 * For now, autocomplete is not included, but is planned for future iterations.
 * All search results are displayed within RecyclerView after they are fetched from API.
 */
public class AddTrackFragment extends BaseFragment {

    public static final String TAG = "com.ingloriouscoders.tags.AddTrackFragment";

    @InjectView(R.id.etTrackSearch)
    EditText etTrackSearch;
    @InjectView(R.id.listTracks)
    RecyclerView listTracks;

    private TextChangedListener etListener;
    private LightRecyclerAdapter2 adapter;
    private TrackController trackController;
    private GroupActivity activity;
    private boolean ready = false;
    private Track trackChosen;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_add_track;
    }

    @Override
    public void main() {
        UBus.registerSafe(this);
        etListener = new TextChangedListener();
        trackController = new TrackController();
        listTracks.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new LightRecyclerAdapter2();
        listTracks.setAdapter(adapter);
        etTrackSearch.addTextChangedListener(etListener);
        etTrackSearch.setOnEditorActionListener(etListener);
        if (!ready) {
            UBus.getInstance().post(this);
            ready = true;
        }
    }

    /**
     * Triggered after the text has been changed in EditText
     *
     * @param action Action describing TextChanged event
     */
    @Subscribe
    public void onTextChanged(TextChanged action) {
        if (action.getText().length() > 2)
            trackController.queryTracks(action.getText(), activity);
    }

    /**
     * Triggered after the track query returns response
     *
     * @param response Response for Track Query
     */
    @Subscribe
    public void onTrackQueryResponse(TrackQueryResponse response) {
        adapter.clear();
        listTracks.removeAllViews();
        for (Track track : response.getResult()) {
            if (track != null)
                adapter.addPresenter(new TrackPresenter(track, false));
        }
    }

    /**
     * Triggered after the specific track from query result has been chosen
     *
     * @param trackPresenter Presenter for the track being chosen
     */
    @Subscribe
    public void onTrackChosen(TrackPresenter trackPresenter) {
        this.trackChosen = trackPresenter.getTrack();
        TrackController.addTrackToGroup(activity.getPlayController().getGroup(), trackChosen, getBaseActivity());
        View currentFocus = getBaseActivity().getCurrentFocus();
        etTrackSearch.getText().clear();
        if (currentFocus != null)
            DeviceUtil.hideSoftInputKeyboard(currentFocus, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * Triggered after the track has been added to the web database
     *
     * @param addTrackResponse Response from API receiving the track
     */
    @Subscribe
    public void onTrackAdded(AddTrackResponse addTrackResponse) {
        UBus.getInstance().post(new TrackChosen(addTrackResponse.getMessage()));
        USocket.getInstance().emitAddTrack(addTrackResponse);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        UBus.registerSafe(this);
        this.activity = (GroupActivity) activity;
        if (this.etTrackSearch != null)
            etTrackSearch.setText("");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        UBus.unregisterSafe(this);
    }
}