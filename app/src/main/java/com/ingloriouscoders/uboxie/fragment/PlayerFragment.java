package com.ingloriouscoders.uboxie.fragment;

import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.ProgressBarDeterminate;
import com.ingloriouscoders.uboxie.GroupActivity;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.data.controller.ImageController;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.model.Track;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.other.TrackChosen;
import com.ingloriouscoders.uboxie.event.player.SeekStatus;
import com.ingloriouscoders.uboxie.event.player.action.PlayAction;
import com.ingloriouscoders.uboxie.util.C;
import com.ingloriouscoders.uboxie.util.DeviceUtil;
import com.ingloriouscoders.uboxie.util.Uboxie;
import com.lightandroid.util.LightFont;
import com.lightandroid.util.LightUtil;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by David on 9.11.2014..
 * <p/>
 * Fragment containing the player and displaying the current track being played
 * It is the main fragment within GroupActivity
 */
public class PlayerFragment extends BaseFragment implements GroupPlayerFragment {

    public static final String TAG = "com.uboxie.fragments.PlayerFragment";

    // View references
    @InjectView(R.id.btnDrawerToggle)
    TextView btnDrawerToggle;
    @InjectView(R.id.btnMute)
    TextView btnMuteToggle;
    @InjectView(R.id.imgAlbumCoverBlurred)
    ImageView imgAlbumCoverBlurred;
    @InjectView(R.id.imgAlbumCover)
    ImageView imgAlbumCover;
    @InjectView(R.id.txtArtistName)
    TextView txtArtistName;
    @InjectView(R.id.txtTrackName)
    TextView txtTrackName;
    @InjectView(R.id.progressDeterminate)
    ProgressBarDeterminate pbPlayingStatus;
    @InjectView(R.id.txtTime)
    TextView txtTime;

    //Other attributes
    private ImageController imageController;
    private Track track;
    private boolean isMuted = false;
    private GroupActivity activity;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_player;
    }

    @Override
    public void main() {
        UBus.registerSafe(this);
        GroupActivity activity = (GroupActivity) getBaseActivity();
        btnDrawerToggle.setOnClickListener(activity.getDrawerToggleListener());
        imageController = new ImageController();
        this.activity = (GroupActivity) getActivity();
        LightFont.setFont(C.ICONFONT_MATERIAL, btnDrawerToggle, btnMuteToggle);
        onSeekToggle(new SeekStatus(true));
        UBus.getInstance().post(this);
    }


    /**
     * Displays given track information (cover image, artist and title)
     *
     * @param track Track to be displayed
     */
    public void display(Track track) {
        this.track = track;
        imageController.loadImage(track.getIcon(), imgAlbumCoverBlurred, imgAlbumCover);
        txtTrackName.setText(track.getName());
        txtArtistName.setText(track.getArtist());
        pbPlayingStatus.setMin(0);
        pbPlayingStatus.setMax(track.getDuration());
    }

    @Override
    public void display(Group group) {
        display(group.getCurrentSong());
    }

    /**
     * Toggles music volume on/off
     */
    @OnClick(R.id.btnMute)
    void mute() {
        isMuted = !isMuted;
        DeviceUtil.toggleMusicMute(getBaseActivity(), isMuted);
        if (isMuted)
            btnMuteToggle.setText(getText(R.string.icon_volume_off));
        else
            btnMuteToggle.setText(getText(R.string.icon_volume_up));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.track != null && !txtArtistName.getText().equals(""))
            display(track);
    }

    /**
     * Getter for current track
     *
     * @return Current track
     */
    public Track getTrack() {
        return track;
    }

    public void progressUpdate(long progress) {
        txtTime.setText(Uboxie.trackTimeFormat(progress));
        pbPlayingStatus.setProgress(LightUtil.castLongToIntSafe(progress / 1000));
    }


    /**
     * Triggered after the track starts playing
     * Displays the track being played
     *
     * @param action Object containing necessary data about track playing action
     */
    @Subscribe
    public void onTrackPlaying(PlayAction action) {
        display(action.getTrack());
    }

    /**
     * Triggered after the track seeking starts/stops
     *
     * @param seekStatus Object containing data about current track seeking action
     */
    @Subscribe
    public void onSeekToggle(SeekStatus seekStatus) {
//        if (seekStatus.isSeeking())
//            showProgress(getString(R.string.lbl_progress_connecting_group));
//        else
//            dismissProgress();
    }

    /**
     * Triggered after the track has been chosen from AddTrackFragment
     *
     * @param action Object containing data about chosen track
     */
    @Subscribe
    public void onTrackChosen(TrackChosen action) {
//        display(activity.getPlayController().getGroup());
    }

}
