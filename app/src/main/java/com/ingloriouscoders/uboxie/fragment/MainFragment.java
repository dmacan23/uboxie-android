package com.ingloriouscoders.uboxie.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.core.deezer.DeezerUserManager;
import com.ingloriouscoders.uboxie.data.controller.GroupController;
import com.ingloriouscoders.uboxie.data.model.Group;
import com.ingloriouscoders.uboxie.data.presenter.GroupPresenter;
import com.ingloriouscoders.uboxie.event.UBus;
import com.ingloriouscoders.uboxie.event.click.DialogClick;
import com.ingloriouscoders.uboxie.event.other.UboxieError;
import com.ingloriouscoders.uboxie.event.socket.OnGroupAddedEvent;
import com.ingloriouscoders.uboxie.event.socket.OnGroupChangedEvent;
import com.ingloriouscoders.uboxie.network.USocket;
import com.ingloriouscoders.uboxie.network.response.GroupCreatedResponse;
import com.ingloriouscoders.uboxie.ui.LightRecyclerAdapter2;
import com.lightandroid.type.LightIconDrawable;
import com.lightandroid.util.LightFont;
import com.melnykov.fab.FloatingActionButton;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by David on 6.12.2014..
 * <p/>
 * This fragment displays grid of groups from the Uboxie API
 * TODO Searchable groups
 * TODO Pagination loading
 */
public class MainFragment extends BaseFragment {

    @InjectView(R.id.gvGroups)
    RecyclerView gvGroups;
    @InjectView(R.id.fab)
    FloatingActionButton fab;
    @InjectView(R.id.btnInfo)
    TextView btnInfo;
    @InjectView(R.id.btnLogout)
    TextView btnLogout;
    private LightRecyclerAdapter2 adapter;
    private boolean registered;

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void main() {
        if (!registered) {
            UBus.registerSafe(this);
            registered = true;
            USocket.getInstance().listenOnGroupAdded(getActivity());
        }
        adapter = new LightRecyclerAdapter2();
        gvGroups.setLayoutManager(new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.grid_columns)));
        gvGroups.setAdapter(adapter);
        GroupController.getGroups(0, 10);
        fab.attachToRecyclerView(gvGroups);
        fab.setImageDrawable(new LightIconDrawable(getActivity(), getString(R.string.mic_create)).colorRes(R.color.txt_white).sizeDp(24));
        LightFont.setFont("material.ttf", btnInfo, btnLogout);
        showProgress(getString(R.string.lbl_progress_groups_loading));
    }

    /**
     * Triggered after the user selects a GroupPresenter from the grid
     *
     * @param presenter GroupPresenter clicked
     */
    @Subscribe
    public void onItemClick(GroupPresenter presenter) {
        openGroup(presenter.getGroup());
    }

    private void openGroup(Group group) {
        startActivity(GroupController.createOpenGroupIntent(group, getActivity()));
    }

    /**
     * Triggered after the groups have been loaded
     * Creates presenters for each group and stores them within the RecyclerView
     *
     * @param groups Array of groups loaded from the query response
     */
    @Subscribe
    public void onGroupsLoaded(Group[] groups) {
        adapter.clear();
        for (Group group : groups)
            adapter.addPresenter(new GroupPresenter(group));
        dismissProgress();
    }

    /**
     * Triggered if an error occurs within fragment.
     * Dismisses the progress if any is active and displays error message
     *
     * @param error Error object containing the description of error
     */
    @Subscribe
    public void onError(UboxieError error) {
        dismissProgress();
        showMessage(error.getDescription());
    }

    /**
     * Triggered on clicking FloatingActionButton to create group
     * Shows group creation dialog
     */
    @OnClick(R.id.fab)
    public void createGroup() {
        showDialog(R.layout.dialog_create_group, R.string.lbl_new_group, R.string.lbl_create);
    }

    /**
     * Triggered after a click event is triggered within dialog
     * Dismisses the dialog and sends new group creation query to the web API
     *
     * @param click Object containing the data about dialog click
     */
    @Subscribe
    public void onDialogClick(DialogClick click) {
        dismissDialog();
        GroupController.createGroup(((EditText) click.getDialog().findViewById(R.id.etGroupName)).getText().toString(), getActivity());
    }

    /**
     * Triggered after the group has been created
     * Displays progress and loads array of groups again
     *
     * @param response Group created response
     */
    @Subscribe
    public void onGroupCreated(GroupCreatedResponse response) {
//        openGroup(response.getGroup());
    }

    @Subscribe
    public void onGroupAdded(OnGroupAddedEvent event) {
        this.adapter.addPresenter(new GroupPresenter(event.getNewGroup()));
    }

    @Override
    public void onResume() {
        super.onResume();
        UBus.registerSafe(this);
    }

    @Subscribe
    public void onGroupChanged(OnGroupChangedEvent event) {
        List<GroupPresenter> presenters = (List<GroupPresenter>) (List<?>) this.adapter.getPresenters();
        if (presenters != null)
            for (GroupPresenter presenter : presenters)
                if (presenter.getGroup().getCurrentTrack().getId().equals(event.getNewGroup().getId())) {
                    presenter.updateGroup(event.getNewGroup());
                    this.adapter.notifyDataSetChanged();
                }
    }

    @OnClick(R.id.btnInfo)
    public void displayInfoDialog() {
        showDialog(R.layout.dialog_info, R.string.lbl_about, R.string.lbl_ok, null);
    }

    @OnClick(R.id.btnLogout)
    public void askForLogout() {
        showDecision((R.string.lbl_logout), (R.string.lbl_logout_description), R.string.lbl_negative, R.string.lbl_positive, new CustomDialog.ClickListener() {
            @Override
            public void onConfirmClick() {
                new DeezerUserManager(getActivity()).clearUserData(getActivity());
                getActivity().finish();
            }

            @Override
            public void onCancelClick() {

            }
        });
    }

}
