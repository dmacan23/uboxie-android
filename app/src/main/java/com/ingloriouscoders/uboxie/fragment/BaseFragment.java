package com.ingloriouscoders.uboxie.fragment;

import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.github.mrengineer13.snackbar.SnackBar;
import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.event.click.DialogClickListener;
import com.lightandroid.navigation.fragment.LightFragment;

import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by dmacan on 12/24/14.
 * <p/>
 * Abstract level fragment which is extended by all other fragments within Uboxie app.
 * Contains methods for common usage throughout the application, such as message displaying, progress handling etc.
 * Using this fragment, all messages, dialogs, progress bars etc. look the same throughout the whole application, which is great for UX
 */
public abstract class BaseFragment extends LightFragment {

    private com.github.mrengineer13.snackbar.SnackBar snackBar;
    private View defaultView;
    private View progressView;
    private CustomDialog customDialog;
    private Activity activity;

    /**
     * Displays message text within the fragment
     *
     * @param message Message to be displayed
     */
    public void showMessage(String message) {
        if (this.defaultView == null)
            this.defaultView = getView();
        new com.github.mrengineer13.snackbar.SnackBar.Builder(activity.getApplicationContext(), defaultView).withMessage(message).withDuration(com.github.mrengineer13.snackbar.SnackBar.SHORT_SNACK).withStyle(SnackBar.Style.INFO).show();
    }

    /**
     * Displays progress bar with given message
     *
     * @param message Message to be displayed
     */
    public void showProgress(String message) {
        if (this.defaultView == null)
            this.defaultView = getView();
        setView(R.layout.layout_progress);
        ((TextView) progressView.findViewById(R.id.txtProgressMessage)).setText(message);
    }


    /**
     * Dismisses progress and returns default view
     */
    public void dismissProgress() {
        if (defaultView != null && progressView != null) {
            setView(defaultView);
            progressView = null;
        }
    }

    /**
     * Displays given View within fragment
     *
     * @param view View to be displayed
     */
    public void setView(View view) {
        if (progressView == null)
            progressView = view;
        getContainer().removeAllViews();
        getContainer().addView(view);
    }

    /**
     * Displays given View within fragment
     *
     * @param layoutRes Resource identifier for the view to display
     */
    public void setView(int layoutRes) {
        setView(View.inflate(activity, layoutRes, null));
    }

    /**
     * Dismisses the dialog if it is showing
     */
    public void dismissDialog() {
        if (customDialog != null && customDialog.isShowing())
            customDialog.dismiss();
    }

    /**
     * Displays dialog with given view, title and text for bottom-right button
     *
     * @param view     View to be displayed
     * @param title    Title of the dialog
     * @param positive Text to be displayed within bottom-right (positive) button
     */
    public void showDialog(View view, String title, String positive) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity, title, positive);
        builder.titleColorRes(R.color.primary);
        customDialog = builder.build();
        customDialog.setCustomView(view);
        customDialog.setClickListener(new DialogClickListener(customDialog));
        customDialog.show();
    }

    /**
     * Displays dialog with given view, title and text for bottom-right button
     *
     * @param layout   Resource identifier for the view to be displayed
     * @param title    Resource identifier for the title of the dialog
     * @param positive Resource identifier for the text to be displayed within bottom-right (positive) button
     */
    public void showDialog(int layout, int title, int positive) {
        showDialog(View.inflate(activity, layout, null), getResources().getString(title), getResources().getString(positive));
    }

    public void showDialog(int layout, int title, int positive, CustomDialog.ClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity, title, positive);
        builder.titleColorRes(R.color.primary);
        customDialog = builder.build();
        View view = View.inflate(activity, layout, null);
        ((TextView) view.findViewById(R.id.txtDialogInfo)).setMovementMethod(new ScrollingMovementMethod());
        customDialog.setCustomView(view);
        if (listener != null)
            customDialog.setClickListener(listener);
        customDialog.show();
    }

    public void showDecision(int title, int message, int negative, int positive, CustomDialog.ClickListener onClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity, title, positive);
        builder.titleColorRes(R.color.primary);
        builder.negativeText(negative);
        builder.content(getString(message));
        customDialog = builder.build();
        customDialog.setClickListener(onClickListener);
        customDialog.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    /**
     * Returns the activity containing the current fragment
     *
     * @return Activity containing the current fragment
     */
    public Activity getBaseActivity() {
        return activity;
    }
}
