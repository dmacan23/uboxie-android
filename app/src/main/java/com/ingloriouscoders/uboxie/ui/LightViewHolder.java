package com.ingloriouscoders.uboxie.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by dmacan on 12/26/14.
 * <p/>
 * Custom ViewHolder implementation for usage with scalable and modular LightRecyclerAdapter2
 * TODO add to LightAndroid library
 */
public class LightViewHolder extends RecyclerView.ViewHolder {

    private LightRecyclerPresenter presenter;
    private View view;

    public LightViewHolder(View itemView, LightRecyclerPresenter presenter) {
        super(itemView);
        presenter.inject(itemView);
        this.view = itemView;
        this.presenter = presenter;
    }

    public LightRecyclerPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(LightRecyclerPresenter presenter) {
        this.presenter = presenter;
    }

    public View getView() {
        return view;
    }
}
