package com.ingloriouscoders.uboxie.ui;

import android.view.View;

/**
 * Created by dmacan on 12/26/14.
 * <p/>
 * Interface for presenting items within the RecyclerView using LightRecyclerAdapter2
 * TODO add to LightAndroid library
 */
public interface LightRecyclerPresenter {

    /**
     * Initializes and injects the current view and its children
     *
     * @param view View to inject
     */
    public void inject(View view);

    /**
     * Displays data within the current view and its children
     *
     * @param view     View being displayed
     * @param position Position of the view within the list
     */
    public void display(View view, int position);

    /**
     * Provides resource for layout to inflate view with
     *
     * @return Resource identifier for the wanted layout
     */
    public int getLayoutRes();

}
