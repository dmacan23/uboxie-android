package com.ingloriouscoders.uboxie.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dmacan on 12/26/14.
 * <p/>
 * Second try at creating a scalable and modular adapter for RecyclerView
 * TODO add to LightAndroid
 */
public class LightRecyclerAdapter2 extends RecyclerView.Adapter<LightViewHolder> {

    private List<LightRecyclerPresenter> presenters;

    public LightRecyclerAdapter2(List<LightRecyclerPresenter> presenters) {
        this.presenters = presenters;
    }

    public LightRecyclerAdapter2() {
        this.presenters = new ArrayList<>();
    }

    @Override
    public LightViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(presenters.get(i).getLayoutRes(), viewGroup, false);
        return new LightViewHolder(v, presenters.get(i));
    }

    @Override
    public void onBindViewHolder(LightViewHolder lightViewHolder, int i) {
        lightViewHolder.getPresenter().display(lightViewHolder.getView(), i);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return presenters.size();
    }

    public List<LightRecyclerPresenter> getPresenters() {
        return presenters;
    }

    public void setPresenters(List<LightRecyclerPresenter> presenters) {
        this.presenters = presenters;
        this.notifyDataSetChanged();
    }

    public void addPresenter(LightRecyclerPresenter presenter) {
        this.presenters.add(presenter);
        this.notifyDataSetChanged();
    }

    public void removePresenter(LightRecyclerPresenter presenter) {
        this.presenters.remove(presenter);
        this.notifyDataSetChanged();
    }

    public void removePresenter(int position) {
        this.presenters.remove(position);
        this.notifyDataSetChanged();
    }

    public void addMultiplePresenters(LightRecyclerPresenter... presenters) {
        this.presenters.addAll(new ArrayList<LightRecyclerPresenter>(Arrays.asList(presenters)));
        this.notifyDataSetChanged();
    }

    public void addMultiplePresenters(List<LightRecyclerPresenter> presenters) {
        this.presenters.addAll(presenters);
        this.notifyDataSetChanged();
    }

    public LightRecyclerPresenter getPresenter(int position) {
        return this.presenters.get(position);
    }

    public void clear() {
        this.presenters.clear();
        this.notifyDataSetChanged();
    }
}
