package com.ingloriouscoders.uboxie.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by dmacan on 12/26/14.
 * <p/>
 * Presenter object for RecyclerView
 * TODO add to LightAndroid library
 */
public abstract class LightPresenter extends RecyclerView.ViewHolder {

    public LightPresenter(View itemView) {
        super(itemView);
    }

    public abstract int getLayoutRes();
}
