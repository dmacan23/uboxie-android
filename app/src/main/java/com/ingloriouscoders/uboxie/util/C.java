package com.ingloriouscoders.uboxie.util;

/**
 * Created by David on 5.10.2014..
 * <p/>
 * This class contains some constants used throughout the application
 */
public class C {

    public static final boolean isDebugMode = true;
    public static final String ICONFONT = "uboxie.ttf";
    public static final String ICONFONT_MATERIAL = "material.ttf";

    public static final int AUTOCOMPLETE_RESULT_NUMBER = 5;
    public static final String DATETIME_MONGO = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static class Api {
        public static final String ROOT = "http://uboxie.me";
        public static final String QUERY_AUTOCOMPLETE = "status,result(name,key,artist)";
        public static final String QUERY_TRACK = "status,result(name,key,artist,icon,album,duration)";
    }

    public static class Key {
        public static final String GROUP_SELECTED = "com.uboxie.group.SELECTED";
        public static final String PREFERENCES = "com.uboxie.prefs.PRIVATE";
        public static final String USER = "com.uboxie.prefs.USER";
    }

}
