package com.ingloriouscoders.uboxie.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ingloriouscoders.uboxie.network.UboxieAPI;

import org.joda.time.DateTime;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by David on 8.12.2014..
 * <p/>
 * Class containing various utility methods to use throughout the application
 */
public class Uboxie {

    private static RestAdapter restAdapter;
    private static Gson gson;

    public static SharedPreferences.Editor loadEditor(Context context, String key) {
        return loadPreferences(context, key).edit();
    }

    public static SharedPreferences loadPreferences(Context context, String key) {
        return context.getSharedPreferences(key, Context.MODE_PRIVATE);
    }

    public static String trackTimeFormat(long milliseconds) {
        DateTime dt = new DateTime(milliseconds);
        return dt.toString("mm:ss");
    }

    public static UboxieAPI API() {
        return REST().create(UboxieAPI.class);
    }

    public static RestAdapter REST() {
        if (restAdapter == null) {
            restAdapter = new RestAdapter.Builder().setConverter(new GsonConverter(GSON())).setEndpoint(UboxieAPI.ENDPOINT).setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                @Override
                public void log(String message) {
                    Log.d(UboxieAPI.TAG, message);
                }
            }).build();
        }
        return restAdapter;
    }

    public static Gson GSON() {
        if (gson == null)
            gson = new GsonBuilder().disableHtmlEscaping().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).enableComplexMapKeySerialization().setPrettyPrinting().create();
        return gson;
    }

    public static String serialize(Object object) {
        return GSON().toJson(object);
    }

    public static <T> T deserialize(String object, Class<T> tClass) {
        return GSON().fromJson(object, tClass);
    }

    public static JsonObject serializeToJSONObject(Object object) {
        JsonElement element = GSON().toJsonTree(object);
        return element.getAsJsonObject();
    }

}
