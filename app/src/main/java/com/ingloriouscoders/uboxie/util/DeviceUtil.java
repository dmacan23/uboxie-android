package com.ingloriouscoders.uboxie.util;

import android.content.Context;
import android.graphics.Point;
import android.media.AudioManager;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by dmacan on 10.11.2014..
 * <p/>
 * Utility methods concerning general device problems
 */
public class DeviceUtil {

    /**
     * Returns dimensions of the screen
     *
     * @param context Context being used
     * @return Array containing width at first and height at second position
     */
    public static int[] getScreenDimens(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point p = new Point();
        display.getSize(p);
        return new int[]{p.x, p.y};
    }

    /**
     * Returns only a percentage of screen width
     *
     * @param percentage Percentage value to calculate width with
     * @param context    Context being used
     * @return Part (percentage) of screen width
     */
    public static int getPartOfScreenWidth(double percentage, Context context) {
        percentage = percentage / 100;
        return ((int) (getScreenDimens(context)[0] * percentage));
    }

    /**
     * Toggles music volume within the application on/off
     *
     * @param context Context being used
     * @param on      If true, mutes the volume. If false, returns the volume as it was
     */
    public static void toggleMusicMute(Context context, boolean on) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, on);
    }

    public static void hideSoftInputKeyboard(View view, int flag) {
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), flag);
    }

}
