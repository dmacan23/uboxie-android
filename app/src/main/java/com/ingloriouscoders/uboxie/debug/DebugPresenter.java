package com.ingloriouscoders.uboxie.debug;

import android.view.View;
import android.widget.TextView;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.ui.LightRecyclerPresenter;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by dmacan on 12/26/14.
 */
public class DebugPresenter implements LightRecyclerPresenter {

    @InjectView(R.id.txtValue)
    TextView txtValue;

    private DebugObject object;

    public DebugPresenter(DebugObject object) {
        this.object = object;
    }

    @Override
    public void inject(View view) {
        ButterKnife.inject(this, view);
    }

    @Override
    public void display(View view, int position) {
        txtValue.setText(object.getValue());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_debug_presenter;
    }
}
