package com.ingloriouscoders.uboxie.debug;

import android.view.View;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.ui.LightRecyclerPresenter;

/**
 * Created by dmacan on 12/26/14.
 */
public class DebugPresenter2 implements LightRecyclerPresenter {
    @Override
    public void inject(View view) {

    }

    @Override
    public void display(View view, int position) {

    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_debug_presenter2;
    }
}
