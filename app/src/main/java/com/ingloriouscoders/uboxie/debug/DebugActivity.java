package com.ingloriouscoders.uboxie.debug;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ingloriouscoders.uboxie.R;
import com.ingloriouscoders.uboxie.ui.LightRecyclerAdapter2;
import com.ingloriouscoders.uboxie.ui.LightRecyclerPresenter;
import com.lightandroid.navigation.activity.LightActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by David on 2.12.2014..
 */
public class DebugActivity extends LightActivity {

    @InjectView(R.id.recycler)
    RecyclerView recyclerView;
    LightRecyclerAdapter2 adapter;

    @Override
    public int provideLayoutRes() {
        return R.layout.layout_recycler;
    }

    @Override
    public void main() {
        recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(), getResources().getInteger(R.integer.grid_columns)));
        adapter = new LightRecyclerAdapter2(presenters());
        recyclerView.setAdapter(adapter);
    }

    private List<LightRecyclerPresenter> presenters() {
        List<LightRecyclerPresenter> presenters = new ArrayList<>();
        for (int i = 0; i < 50; i++)
            presenters.add(new DebugPresenter(new DebugObject("Object " + i)));
        return presenters;
    }

}
