package com.ingloriouscoders.uboxie.debug;

/**
 * Created by dmacan on 12/26/14.
 */
public class DebugObject {

    private String value;

    public DebugObject(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
