package com.lightandroid.ui.presenter;

/**
 * Created by David on 8.10.2014..
 */
public interface LightAdapterItemFilterable extends LightAdapterItem {
    public String[] getFilterableData();
}
