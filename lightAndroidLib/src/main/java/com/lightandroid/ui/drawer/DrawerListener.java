package com.lightandroid.ui.drawer;

/**
 * Created by David on 16.11.2014..
 */
public interface DrawerListener {

    public void onDrawerItemSelected(int position);

}
