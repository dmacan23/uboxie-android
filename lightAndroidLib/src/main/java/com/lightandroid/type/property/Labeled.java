package com.lightandroid.type.property;

/**
 * Created by David on 16.11.2014..
 */
public interface Labeled {

    public String provideLabel();

}
