package com.lightandroid.type.property;

import android.graphics.drawable.Drawable;

/**
 * Created by David on 16.11.2014..
 */
public interface Iconified {

    public Drawable provideIcon();

}
