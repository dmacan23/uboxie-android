package com.lightandroid.event;

import android.support.v4.app.Fragment;

/**
 * Created by David on 7.12.2014..
 */
public interface OnFragmentReadyListener {

    public void onFragmentReady(Fragment fragment, String tag, int id);

}
